//
//  SettingsController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 22/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class SettingsViewController: BluetoothViewController {
    deinit
    {
        print("deinit ---- SettingsViewController")
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var userJoinedView: MyNotificationSettingsView!
    @IBOutlet weak var userConnectedView: MyNotificationSettingsView!
    @IBOutlet weak var userWarningView: MyNotificationSettingsView!
    @IBOutlet weak var userLostView: MyNotificationSettingsView!
    @IBOutlet weak var userDisconnectedView: MyNotificationSettingsView!
    @IBOutlet weak var messageNotifyView: MyNotificationSettingsView!
    @IBOutlet weak var usernameChangedNotifyView: MyNotificationSettingsView!

    //MARK:- View functions
    override func viewDidLoad()
    {
        super.viewDidLoad()
        userJoinedView.set(settingActivated: Constants.joinedNotify)
        userConnectedView.set(settingActivated: Constants.connectedNotify)
        userWarningView.set(settingActivated: Constants.warningNotify)
        userLostView.set(settingActivated: Constants.lostNotify)
        userDisconnectedView.set(settingActivated: Constants.disconnectedNotify)
        messageNotifyView.set(settingActivated: Constants.messageNotify)
        usernameChangedNotifyView.set(settingActivated: Constants.usernameChangedNotify)
        
        userNameTextField.text = WebService.sharedInstance.currentUser?.username
        userNameTextField.layer.cornerRadius = userNameTextField.frame.size.height / 2
        cancelButton.layer.cornerRadius = cancelButton.frame.size.height / 2
        saveButton.layer.cornerRadius = saveButton.frame.size.height / 2
        userNameTextField.layer.borderWidth = 1
        userNameTextField.clipsToBounds = true
        cancelButton.clipsToBounds = true
        cancelButton.layer.borderColor = UIColor(red: 39/255.0, green: 160/255.0, blue: 255/255.0, alpha: 1).cgColor
        cancelButton.layer.borderWidth = 1
        saveButton.clipsToBounds = true
        settingsView.layer.cornerRadius = 14
        settingsView.clipsToBounds = true
        
        if !Constants.currentReachability
        {
            saveButton.isEnabled = false
        }
        else
        {
            saveButton.isEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        notificationWithInsertForMessages = false
        notificationWithInsertForUsers = false
        notificationListenerUsers()
        notificationListenerMessages()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Create view controller
    static func createViewController() -> SettingsViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        return controller
    }
    
    //MARK:- Actions
    @IBAction func back()
    {
        notificationBar.setLocationNotificationBar()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func save()
    {
        if let username = userNameTextField.text, !(username.isEmpty)
        {
            let usernameWithoutSpaces = username.split(separator: " ")
            if usernameWithoutSpaces.count == 0
            {
                self.showErrors(title: "User name error", bodyText: "Username must contain at least one unblank character!", theme: .warning)
                return
            }
            userNameTextField.resignFirstResponder()
            self.view.showLoadingIndicator()
            if Constants.currentReachability
            {
                WebService.sharedInstance.updateUserName(username: username) { (response) in
                    guard response.isSuccess else {
                        self.view.hideLoadingIndicator(animated: true)
                        self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to update user name!", theme: .error)
                        return
                    }
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(self.userJoinedView.settingActivated, forKey: "joinedNotify")
                    UserDefaults.standard.set(self.userConnectedView.settingActivated, forKey: "connectedNotify")
                    UserDefaults.standard.set(self.userWarningView.settingActivated, forKey: "warningNotify")
                    UserDefaults.standard.set(self.userLostView.settingActivated, forKey: "lostNotify")
                    UserDefaults.standard.set(self.userDisconnectedView.settingActivated, forKey: "disconnectedNotify")
                    UserDefaults.standard.set(self.messageNotifyView.settingActivated, forKey: "messageNotify")
                    UserDefaults.standard.set(self.usernameChangedNotifyView.settingActivated, forKey: "usernameChangedNotify")
                    
                    Constants.joinedNotify = self.userJoinedView.settingActivated
                    Constants.connectedNotify = self.userConnectedView.settingActivated
                    Constants.warningNotify = self.userWarningView.settingActivated
                    Constants.lostNotify = self.userLostView.settingActivated
                    Constants.disconnectedNotify = self.userDisconnectedView.settingActivated
                    Constants.messageNotify = self.messageNotifyView.settingActivated
                    Constants.usernameChangedNotify = self.usernameChangedNotifyView.settingActivated
                    
                    self.view.hideLoadingIndicator(animated: false)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        else
        {
            self.showErrors(title: "User name missing", bodyText: "Complete user name field!", theme: .warning)
        }
    }

    //MARK:- Functions
    @objc override func didChangeConnexion(notification: Notification)
    {
        guard let hasConnection = notification.object as? Bool else {
            return
        }
        
        Constants.currentReachability = hasConnection
        
        if !hasConnection
        {
            saveButton.isEnabled = false
            showInternetConnectionError()
        }
        else
        {
            saveButton.isEnabled = true
            hideInternetConnectionError()
        }
    }
    
    func hideTextView()
    {
        userNameTextField.resignFirstResponder()
        userNameTextField.isEnabled = false
        saveButton.isEnabled = false
    }
    
    func showTextView()
    {
        userNameTextField.isEnabled = true
        saveButton.isEnabled = true
    }
    
    override func showInternetConnectionError()
    {
        super.showInternetConnectionError()
        hideTextView()
    }
    
    override func hideInternetConnectionError()
    {
        super.hideInternetConnectionError()
        showTextView()
    }
}

extension SettingsViewController: UITextFieldDelegate
{
    //MARK: - Text Field Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
