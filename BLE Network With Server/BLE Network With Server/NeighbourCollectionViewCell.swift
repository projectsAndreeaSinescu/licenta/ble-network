//
//  NeighbourCollectionViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 03/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kNeighbourCollectionViewCell = "NeighbourCollectionViewCell"

class NeighbourCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var neighbourView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
