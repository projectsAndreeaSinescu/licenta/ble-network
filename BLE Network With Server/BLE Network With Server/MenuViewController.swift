//
//  MenuController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 10/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class MenuViewController: BluetoothViewController {
    deinit
    {
        print("deinit ---- MenuViewController")
        self.removeNotificationListener()
    }
    
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var unreadMessagesCounterView: UIView!
    @IBOutlet weak var unreadMessagesCounterLabel: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        settingsButton.layer.borderWidth = 1
        settingsButton.layer.borderColor = UIColor.white.cgColor
        settingsButton.layer.cornerRadius = settingsButton.frame.size.height / 2
        settingsButton.clipsToBounds = true
        listView.layer.cornerRadius = listView.frame.size.height / 2
        listView.clipsToBounds = true
        chatView.layer.cornerRadius = chatView.frame.size.height / 2
        chatView.clipsToBounds = true
        mapView.layer.cornerRadius = mapView.frame.size.height / 2
        mapView.clipsToBounds = true
        logoutView.layer.borderColor = UIColor.white.cgColor
        logoutView.layer.borderWidth = 1
        logoutView.layer.cornerRadius = logoutView.frame.size.height / 2
        logoutView.clipsToBounds = true
        
        notificationListenerUsers()
        notificationListenerMessages()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        notificationBar.moveNotificationBar()
        if Constants.unreadMessages > 0
        {
            unreadMessagesCounterView.alpha = 1
            if Constants.unreadMessages > 999
            {
                unreadMessagesCounterLabel.text = "999+"
            }
            else
            {
                unreadMessagesCounterLabel.text = "\(Constants.unreadMessages)"
            }
        }
        else
        {
            unreadMessagesCounterView.alpha = 0
        }
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
    }
    
    //MARK:- Create view controller
    static func createViewController() -> UINavigationController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let navController = UINavigationController(rootViewController: controller)
        navController.isNavigationBarHidden = true
        
        return navController
    }
    
    //MARK:- Actions
    @IBAction func settings()
    {
        self.notificationBar.setLocationNotificationBar()
        let controller = SettingsViewController.createViewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func goToList()
    {
        if Constants.currentReachability
        {
            self.view.showLoadingIndicator()
            WebService.sharedInstance.getUsers { (response) in
                guard response.isSuccess else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to get users!", theme: .error)
                    return
                }
                Constants.users = response.parsedResult as! [User]
                self.view.hideLoadingIndicator(animated: false)

                self.notificationBar.setLocationNotificationBar()
                let controller = GroupViewController.createViewController()
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        else if Constants.users.count != 0
        {
            notificationBar.setLocationNotificationBar()
            let controller = GroupViewController.createViewController()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func goToChat()
    {
        if Constants.currentReachability
        {
            self.view.showLoadingIndicator()
            WebService.sharedInstance.getMessages(completion: { (response) in
                guard response.isSuccess, let messages = response.parsedResult as? [Message] else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to get messages!", theme: .error)
                    return
                }
                self.view.hideLoadingIndicator(animated: false)
                
                Constants.messages = messages
                
                self.notificationBar.setLocationNotificationBar()
                let controller = ChatViewController.createViewController()
                self.navigationController?.pushViewController(controller, animated: true)
            })
        }
        else if Constants.messages.count != 0
        {
            notificationBar.setLocationNotificationBar()
            let controller = ChatViewController.createViewController()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func goToMap()
    {
        if Constants.currentReachability
        {
            self.view.showLoadingIndicator()
            WebService.sharedInstance.getUsers { (response) in
                guard response.isSuccess else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to get users!", theme: .error)
                    return
                }
                Constants.users = response.parsedResult as! [User]
                self.view.hideLoadingIndicator(animated: false)
                
                self.notificationBar.setLocationNotificationBar()
                let controller = MapViewController.createViewController()
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
        else if Constants.users.count != 0
        {
            notificationBar.setLocationNotificationBar()
            let controller = MapViewController.createViewController()
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func logout()
    {
        if Constants.currentReachability
        {
            self.view.showLoadingIndicator()
            WebService.sharedInstance.logout() { (response) in
                guard response.isSuccess else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to logout!", theme: .error)
                    return
                }
                Constants.messages = []
                Constants.notifications = []
                Constants.users = []
                Constants.unreadMessages = 0
                self.view.hideLoadingIndicator(animated: false)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //Functions
    @objc override func refreshMessages(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            if notificationWithInsertForMessages
            {
                Constants.notifications.insert((infoNotification[2], infoNotification[1]), at: 0)
            }
            Constants.unreadMessages = Constants.unreadMessages + 1
            unreadMessagesCounterLabel.text = "\(Constants.unreadMessages)"
            if unreadMessagesCounterView.alpha == 0
            {
                UIView.animate(withDuration: 0.1) {
                    self.unreadMessagesCounterView.alpha = 1
                }
            }
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
        }
    }
}

