//
//  SendNotificationViewController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 21/06/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
protocol SendNotificationViewControllerDelegate: class {
    func cancel()
    func send(type: Int, content: String, image: UIImage)
}

class SendNotificationViewController: UIViewController {

    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var verticalyContraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    
    private weak var delegate: SendNotificationViewControllerDelegate!
    
    var messageType: Int = 0
    var image: UIImage!
    var content: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        cancelButton.layer.cornerRadius = cancelButton.frame.size.height / 2
        sendButton.layer.cornerRadius = sendButton.frame.size.height / 2
        cancelButton.clipsToBounds = true
        cancelButton.layer.borderColor = UIColor(red: 39/255.0, green: 160/255.0, blue: 255/255.0, alpha: 1).cgColor
        cancelButton.layer.borderWidth = 1
        sendButton.clipsToBounds = true
        messageImageView.image = image
        if messageType == 2
        {
            notificationMessageLabel.text = "Are you sure that you want to send this image?"
        }
        else
        {
            notificationMessageLabel.text = "Are you sure that you want to send your coordinates?"
        }
        view.alpha = 0
        verticalyContraint.constant = 100
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 1
            self.verticalyContraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- Create view controller
    static func createViewController(type: Int, image: UIImage, delegate: SendNotificationViewControllerDelegate, content: String?) -> SendNotificationViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SendNotificationViewController") as! SendNotificationViewController
        controller.messageType = type
        controller.image = image
        controller.delegate = delegate
        if let content = content
        {
            controller.content = content
        }
        return controller
    }
    
    //MARK:- Actions
    @IBAction func cancel(_ sender: Any)
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
            self.verticalyContraint.constant = 100
            self.view.layoutIfNeeded()
        }) { (_) in
            self.delegate.cancel()
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func send(_ sender: Any)
    {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
            self.verticalyContraint.constant = 100
            self.view.layoutIfNeeded()
        }) { (_) in
            self.delegate.send(type: self.messageType, content: self.content, image: self.image)
            self.dismiss(animated: false, completion: nil)
        }
    }
}
