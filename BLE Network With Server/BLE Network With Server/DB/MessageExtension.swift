//
//  TextMessageExtension.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import Foundation
import CoreData
import MapKit

//MARK: Web convertions

extension Message
{
    /// Set data from web dictionary
    ///
    /// - Parameter dict: data
    func loadFromDict(_ dict : [String: Any])
    {
        self.uid = dict["id"] as! Int64
        self.content = dict["content"] as? String
        self.type = dict["type"] as! Int64
        if let aspectRatio = dict["aspectRatio"] as? Double
        {
            self.imageAspectRatio = aspectRatio
        }
        if let senderID = dict["senderID"] as? String
        {
            var sender = CoreDataController.sharedInstance.retrieveOrCreateUser(senderID)
            if (sender.username == nil)
            {
                if let senderData = dict["sender"] as? [String : Any]
                {
                    sender = CoreDataController.sharedInstance.createOrReplaceUser(senderData)
                }
            }
            self.sender = sender
        }
        else
        {
            self.sender = nil
        }
    }
    
    /// Build a dictionary based on the data - will be used to create json
    ///
    /// - Returns: the dictionary
    func getDict() -> [String: Any]
    {
        return [:]
    }
}


//MARK: CoreData extensions
extension CoreDataController
{
    func retrieveOrCreateMessage(_ id : Int64) -> Message
    {
        let message = self.retriveOrCreateObject(forEntity: "Message", withField: "uid", matching: id) as! Message
        
        return message
    }
    
    func createOrReplaceMessage(content: String?, id: Int64) -> Message
    {
        let message = self.retriveOrCreateObject(forEntity: "Message",
                                                 withField: "uid",
                                                 matching: id) as! Message
        message.content = content
        
        self.saveContext()
        
        return message
    }
    
    func createOrReplaceMessage(_ dict : [String : Any]) -> Message
    {
        let message = self.retriveOrCreateObject(forEntity: "Message",
                                               withField: "uid",
                                               matching: (dict["id"] as! Int64)) as! Message
        message.loadFromDict(dict)
        
        self.saveContext()
        
        return message
    }
    
    func retrieveMessages() -> [Message]
    {
        let request = NSFetchRequest<Message>()
        let entity = NSEntityDescription.entity(forEntityName: "Message", in: self.managedObjectContext)
        request.entity = entity
        
        do
        {
            let result = try self.managedObjectContext.fetch(request)
            return result
        }
        catch
        {
            LogManager.staticInstance.writeToLogs("Error in \(#function) \(error)")
        }
        
        return [Message]()
    }
    
    
}
