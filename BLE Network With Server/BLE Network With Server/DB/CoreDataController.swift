//
//  CoreDataController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import CoreData

/**
 Manager for CoreData Access.This class is intended to be extended to contain project specific methods. It is not intended to be used directly on another tier, but it can be used
 - warning  - uses mainQueueConcurrencyType
 */
class CoreDataController: NSObject {
    static var sharedInstance = CoreDataController()
    
    // MARK: - Core Data start
    private var _managedObjectModel : NSManagedObjectModel?
    
    var managedObjectModel: NSManagedObjectModel  {
        if(_managedObjectModel == nil)
        {
            
        }
        
        let modelURL = Bundle.main.url(forResource: "BLE_Network_With_Server", withExtension: "momd")
        _managedObjectModel = NSManagedObjectModel(contentsOf: modelURL!)
        
        
        return _managedObjectModel!
        
    }
    
    private var _persistentStoreCoordinator : NSPersistentStoreCoordinator?
    
    var persistentStoreCoordinator: NSPersistentStoreCoordinator  {
        
        get
        {
            if(_persistentStoreCoordinator == nil)
            {
                let storeURL = Utils.documentsDirectory(path: "BLE_Network_With_Server.sqlite")
                let options = [NSMigratePersistentStoresAutomaticallyOption : true,
                               NSInferMappingModelAutomaticallyOption : true]
                
                _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
                
                do
                {
                    try _persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType,
                                                                        configurationName: nil,
                                                                        at: storeURL,
                                                                        options: options)
                }
                catch
                {
                    LogManager.staticInstance.writeToLogs("Error \(#function) -> \(error)")
                    abort()
                }
            }
            
            return _persistentStoreCoordinator!
            
        }
    }
    
    private var _managedObjectContext : NSManagedObjectContext?
    
    var managedObjectContext: NSManagedObjectContext  {
        if(_managedObjectContext == nil)
        {
            _managedObjectContext = NSManagedObjectContext(concurrencyType :NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
            _managedObjectContext?.persistentStoreCoordinator = self.persistentStoreCoordinator
            
        }
        
        return _managedObjectContext!
        
    }
    
    // MARK: - Core Data Saving support
    /**
     Saves the current context - in the event of an exception - the error will de printed
     */
    func saveContext()
    {
        guard managedObjectContext.hasChanges else
        {
            return
        }
        self.managedObjectContext.performAndWait({() -> Void in
            do
            {
                try self.managedObjectContext.save()
            }
            catch let exception {
                print("Error \(exception)")
            }
        })
    }
    
    /**
     Discared pending changes the current context
     */
    func rollbackContext()
    {
        let managedObjectContext = self.managedObjectContext
        managedObjectContext.performAndWait({ () -> Void in
            managedObjectContext.rollback()
        })
    }
    
    
    // Mark: General Items
    /**
     Create a new instance of a managed object.
     - parameter entityName : name of the entity of the new object
     - warning : saves context
     */
    func insertNewObject(forEntityName entityName: String) -> NSManagedObject
    {
        
        let object =  NSEntityDescription.insertNewObject(forEntityName: entityName, into: self.managedObjectContext)
        self.saveContext()
        return object
    }
    
    /**
     Retrieves a managed object, based on a filter. Exactly one object must match the citeria
     - parameter entityName : name of the entity of the object
     - parameter fieldName : name of the field for the search condition
     - parameter fieldValue : value to be matched for the search condition
     - warning : includes pending changes
     - warning : if any error occurs - the error will be printed and it will be sent to the log file
     - warning : if more then one objectg match the criteria this is considered an error
     - warning : caller is responsible for the fieldValue type
     */
    
    func retriveObject(forEntity entityName: String, withField fieldName: String, matching fieldValue: Any) -> NSManagedObject?
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        fetchRequest.entity = entity
        
        let predicate = NSPredicate(format: "%K = %@", fieldName, fieldValue as! CVarArg)
        fetchRequest.predicate = predicate
        fetchRequest.includesPendingChanges = true
        
        do
        {
            let items = try self.managedObjectContext.fetch(fetchRequest)
            
            if items.count == 1
            {
                return items.first as? NSManagedObject
            }
            else if items.count == 0
            {
                return nil
            }
            else
            {
                LogManager.staticInstance.writeToLogs("Duplicate PK.Object:\(entityName) PK value:\(fieldValue)")
                return nil
            }
        }
        catch let exception
        {
            LogManager.staticInstance.writeToLogs("Error \(exception).Object:\(entityName) PK value:\(fieldValue)")
            return nil
        }
        
    }
    
    /**
     Retrieves or creates a managed object, based on a filter. If no object exists a new object will be created and the specified field will be set
     - parameter entityName : name of the entity of the object
     - parameter fieldName : name of the field for the search condition
     - parameter fieldValue : value to be matched for the search condition
     - warning : includes pending changes
     - warning : if any error occurs - the error will be printed and it will be sent to the log file
     - warning : if more then one objectg match the criteria this is considered an error
     - warning : caller is responsible for the fieldValue type
     - warning : saves cotext
     */
    func retriveOrCreateObject(forEntity entityName: String, withField fieldName: String, matching fieldValue: Any) -> NSManagedObject?
    {
        var object = nil as NSManagedObject?
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        fetchRequest.entity = entity
        
        let predicate = NSPredicate(format: "%K = %@", fieldName, "\(fieldValue)")
        fetchRequest.predicate = predicate
        fetchRequest.includesPendingChanges = true
        
        do
        {
            let items = try self.managedObjectContext.fetch(fetchRequest)
            
            if items.count == 1
            {
                object = items.first as? NSManagedObject
            }
            else if items.count == 0
            {
                let newObject = self.insertNewObject(forEntityName: entityName)
                newObject.setValue(fieldValue, forKey: fieldName)
                self.saveContext()
                object = newObject
                
            }
            else
            {
                LogManager.staticInstance.writeToLogs("Duplicate PK.Object:\(entityName) PK value:\(fieldValue)")
            }
        }
        catch let exception
        {
            LogManager.staticInstance.writeToLogs("Error \(exception).Object:\(entityName) PK value:\(fieldValue)")
        }
        
        return object
    }
    
    /**
     Deletes a managed object
     - parameter object : object to be deleted
     - warning : includes pending changes
     - warning : does NOT save context
     */
    func deleteManagedObject(_ object: NSManagedObject)
    {
        self.managedObjectContext.delete(object)
        
    }
    
    /**
     Deletes all managed objects for a given enity
     - parameter entityName : the name of the entity
     - warning : saves context
     */
    func deleteAllObjects(forEntity entityName: String)
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        fetchRequest.entity = entity
        do
        {
            let items = try self.managedObjectContext.fetch(fetchRequest)
            for managedObject in items
            {
                self.managedObjectContext.delete(managedObject as! NSManagedObject)
            }
        }
        catch let exception
        {
            LogManager.staticInstance.writeToLogs("\(#function) Error \(exception)")
        }
        
        self.saveContext()
    }
    
    /**
     Gets next value for a given field. MUST BE INTEGER. E.g.: autoincremented unique ID
     - parameter entityName : entity
     - parameter field : fieldName
     - warning : MUST BE INTEGER
     */
    public func getNextValue(forEntity entityName : String, forField field: String) -> NSNumber
    {
        let entity = NSEntityDescription.entity(forEntityName: entityName, in: self.managedObjectContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entity
        
        let uidOrder = NSSortDescriptor(key: field, ascending: false)
        request.fetchLimit = 1
        request.sortDescriptors = [uidOrder]
        
        do
        {
            let result = try self.managedObjectContext.fetch(request)
            if let last = result.first as? NSManagedObject
            {
                
                if let lastValue = last.value(forKey: field) as? NSNumber
                {
                    return NSNumber(value: lastValue.intValue + 1)
                }
            }
        }
        catch
        {
            LogManager.staticInstance.writeToLogs("Error in \(#function) \(error)")
        }
        
        return 1
    }
}
