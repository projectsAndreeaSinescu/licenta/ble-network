//
//  UserExtension.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import Foundation
import CoreData

extension User
{
    /// Set data from web dictionary
    ///
    /// - Parameter dict: data
    func loadFromDict(_ dict : [String: Any])
    {
        self.username = dict["username"] as? String
        self.deviceID = dict["deviceID"] as? String
        self.firebaseTopic = dict["firebaseTopic"] as? String
        self.bluetoothID = dict["bluetoothID"] as? String
        self.topic = dict["topic"] as? String
        if let status = dict["status"] as? Int64 {
            self.status = status
        }
        if let longitude = dict["longitude"] as? Double, let latitude = dict["latitude"] as? Double
        {
            self.location = "\(latitude),\(longitude)"
        }
        else
        {
            self.location = nil
        }
        if let neighbourDeviceIDsString = dict["neighbours"] as? String
        {
            let neighbourDeviceIDs = neighbourDeviceIDsString.split(separator: ",")
            let neighbours = NSMutableSet(capacity: neighbourDeviceIDs.count)
            for neighbourID in neighbourDeviceIDs
            {
                let neighbour = CoreDataController.sharedInstance.retrieveOrCreateUser(String(neighbourID))
                neighbours.add(neighbour)
            }
            self.neighbours = neighbours
        }
        else
        {
            self.neighbours = nil
        }
    }
    
    /// Build a dictionary based on the data - will be used to create json
    ///
    /// - Returns: the dictionary
    func getDict() -> [String: Any]
    {
        let dict = ["username": self.username!, "deviceID": self.deviceID!, "firebaseTopic": self.firebaseTopic!, "topic": self.topic!, "bluetoothID": self.bluetoothID!, "status": self.status] as [String : Any]
        return dict
    }
}

//MARK: CoreData extensions
extension CoreDataController
{
    func retrieveOrCreateUser(_ deviceID : String) -> User
    {
        let room = self.retriveOrCreateObject(forEntity: "User", withField: "deviceID", matching: deviceID) as! User
        
        return room
    }
    
    func createOrReplaceUsers(_ dicts : [[String : Any]]) -> [User]
    {
        var users = [User]()
        for dict in dicts
        {
            let user = self.retriveOrCreateObject(forEntity: "User",
                                                  withField: "deviceID",
                                                  matching: (dict["deviceID"] as! String)) as! User
            user.loadFromDict(dict)
            users.append(user)
        }
        self.saveContext()
        
        return users
    }
    
    
    func createOrReplaceUser(_ dict : [String : Any]) -> User
    {
        let user = self.retriveOrCreateObject(forEntity: "User",
                                              withField: "deviceID",
                                              matching: (dict["deviceID"] as! String)) as! User
        user.loadFromDict(dict)
        self.saveContext()
        return user
    }
    
    
    func retrieveUsers() -> [User]
    {
        let request = NSFetchRequest<User>()
        let entity = NSEntityDescription.entity(forEntityName: "User", in: self.managedObjectContext)
        request.entity = entity
        
        do
        {
            let result = try self.managedObjectContext.fetch(request)
            return result
        }
        catch
        {
            LogManager.staticInstance.writeToLogs("Error in \(#function) \(error)")
        }
        
        return [User]()
    }
    
    
}
