//
//  AppDelegate.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import UserNotifications

enum NotificationType: String {
    case bleUserJoined = "1"
    case bleUserConnected = "2"
    case bleUserWarning = "3"
    case bleUserLost = "4"
    case bleUserDisconnected = "5"
    case textMessage = "6"
    case imageMessage = "7"
    case coordinatesMessage = "8"
    case usernameChanged = "9"
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
        application.registerForRemoteNotifications()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        guard let notificationTypeString = userInfo["notificationType"] as? String, let notificationType = NotificationType(rawValue: notificationTypeString) else {
                return
                
        }
        
        switch notificationType {
        case .bleUserJoined, .bleUserConnected, .bleUserWarning, .bleUserLost, .bleUserDisconnected:
            if WebService.sharedInstance.currentUser != nil
            {
                if let content = userInfo["body"] as? String
                {
                    if (notificationType == .bleUserJoined && Constants.joinedNotify) || (notificationType == .bleUserConnected && Constants.connectedNotify) || (notificationType == .bleUserWarning && Constants.warningNotify) || (notificationType == .bleUserLost && Constants.lostNotify) || (notificationType == .bleUserDisconnected && Constants.disconnectedNotify)
                    {
                        scheduleNotification(contentText: content)
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
            }
        case .textMessage, .imageMessage, .coordinatesMessage:
            if let messageID = userInfo["messageID"] as? String, let senderID = userInfo["senderID"] as? String, senderID != WebService.sharedInstance.currentUser?.deviceID
            {
                if let content = userInfo["body"] as? String
                {
                    if Constants.messageNotify
                    {
                        scheduleNotification(contentText: content)
                    }
                }
                NotificationCenter.default.post(name: NSNotification.Name("newMessage"), object: messageID)
            }
        case .usernameChanged:
            if let stringUser = userInfo["user"] as? String, let data = stringUser.data(using: String.Encoding.utf8)
            {
                do
                {
                    if let dictionaryUser =  try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    {
                        let user = WebService.sharedInstance.currentUser
                        if let deviceId = dictionaryUser["deviceID"] as? String
                        {
                            let _ = CoreDataController.sharedInstance.createOrReplaceUser(dictionaryUser)
                            if user != nil && user?.deviceID != deviceId
                            {
                                if let content = userInfo["body"] as? String
                                {
                                    if Constants.usernameChangedNotify
                                    {
                                        scheduleNotification(contentText: content)
                                    }
                                }
                                NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
                            }
                        }
                    }
                    
                }
                catch {
                    print(error)
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        guard let notificationTypeString = userInfo["notificationType"] as? String, let notificationType = NotificationType(rawValue: notificationTypeString) else {
            return
            
        }
        
        switch notificationType {
        case .bleUserJoined, .bleUserConnected, .bleUserWarning, .bleUserLost, .bleUserDisconnected:
            if WebService.sharedInstance.currentUser != nil
            {
                if let content = userInfo["body"] as? String
                {
                    if (notificationType == .bleUserJoined && Constants.joinedNotify) || (notificationType == .bleUserConnected && Constants.connectedNotify) || (notificationType == .bleUserWarning && Constants.warningNotify) || (notificationType == .bleUserLost && Constants.lostNotify) || (notificationType == .bleUserDisconnected && Constants.disconnectedNotify)
                    {
                        if let stringUser = userInfo["user"] as? String, let data = stringUser.data(using: String.Encoding.utf8)
                        {
                            do
                            {
                                if let dictionaryUser =  try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                                {
                                    if let deviceId = dictionaryUser["deviceID"] as? String {
                                        if deviceId != WebService.sharedInstance.currentUser?.deviceID
                                        {
                                            scheduleNotification(contentText: content)
                                        }
                                        NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: [deviceId, content, notificationTypeString])
                                    }
                                }
                            }
                            catch
                            {
                                print(error)
                            }
                        }
                        else
                        {
                            scheduleNotification(contentText: content)
                        }
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
                }
                completionHandler(UIBackgroundFetchResult.newData)
            }
        case .textMessage, .imageMessage, .coordinatesMessage:
            if let messageID = userInfo["messageID"] as? String, let senderID = userInfo["senderID"] as? String, senderID != WebService.sharedInstance.currentUser?.deviceID
            {
                if let content = userInfo["body"] as? String
                {
                    if Constants.messageNotify
                    {
                        scheduleNotification(contentText: content)
                        NotificationCenter.default.post(name: NSNotification.Name("newMessage"), object: [messageID, content, notificationTypeString])
                    }
                    else
                    {
                        NotificationCenter.default.post(name: NSNotification.Name("newMessage"), object: messageID)
                    }
                }
                else
                {
                    NotificationCenter.default.post(name: NSNotification.Name("newMessage"), object: messageID)
                }
                completionHandler(UIBackgroundFetchResult.newData)
            }
        case .usernameChanged:
            if let stringUser = userInfo["user"] as? String, let data = stringUser.data(using: String.Encoding.utf8)
            {
                do
                {
                    if let dictionaryUser =  try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    {
                        let user = WebService.sharedInstance.currentUser
                        if let deviceId = dictionaryUser["deviceID"] as? String
                        {
                            let _ = CoreDataController.sharedInstance.createOrReplaceUser(dictionaryUser)
                            if user != nil && user?.deviceID != deviceId
                            {
                                if let content = userInfo["body"] as? String
                                {
                                    if Constants.usernameChangedNotify
                                    {
                                        scheduleNotification(contentText: content)
                                        NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: content)
                                    }
                                    else
                                    {
                                        NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
                                    }
                                }
                                NotificationCenter.default.post(name: NSNotification.Name("refreshUsers"), object: nil)
                                completionHandler(UIBackgroundFetchResult.newData)
                            }
                        }
                    }
                }
                catch
                {
                    print(error)
                }
            }
        }
    }
    
    //MARK:- Users notification
    func scheduleNotification(contentText: String)
    {
        let content = UNMutableNotificationContent()
        content.title = "BLE Network"
        content.body = contentText
        content.sound = UNNotificationSound.default
        let date = Date().addingTimeInterval(2)
        let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second,], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
    
    //MARK:- Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BLE_Network_With_Server")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError?
            {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK:- Core Data Saving support
    func saveContext ()
    {
        let context = persistentContainer.viewContext
        if context.hasChanges
        {
            do
            {
                try context.save()
            }
            catch
            {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate
{
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print(" Token: \(fcmToken)")
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print("\(remoteMessage.appData)")
    }
}
