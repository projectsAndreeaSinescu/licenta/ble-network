//
//  ChatController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 10/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import SwiftMessages

class ChatViewController: BluetoothViewController {
    deinit
    {
        print("deinit ---- ChatViewController")
        self.removeNotificationListener()
    }
    
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var chatButtomView: UIView!
    @IBOutlet weak var verticalPlusLine: UIImageView!
    @IBOutlet weak var sendImageButton: UIButton!
    @IBOutlet weak var sendImageImageView: UIImageView!
    @IBOutlet weak var sendLocationButton: UIButton!
    @IBOutlet weak var sendLocationImageView: UIImageView!
    @IBOutlet weak var sendTextButton: UIButton!
    @IBOutlet weak var contentMessageTextView: UITextView!
    @IBOutlet weak var messagesTableView: UITableView!
    
    @IBOutlet weak var sendLocationVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendImageHorizontalConstraint: NSLayoutConstraint!
    
    var mediaPicker: MyMediaPicker!
    var shouldShowLoading = false
    var firstTime = true
    var emptyCell: MessageTableViewCell!
    var isScrolledToBottom = true
    var image: UIImage?
    
    var cellsHeightArray: [(row: Int, height: CGFloat)] = []
    var tableContentHeight: CGFloat
    {
        return cellsHeightArray.map({$0.height}).reduce(0, +)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        mediaPicker = MyMediaPicker(controller: self, delegate: self)
        self.messagesTableView.alpha = 0
        
        chatView.layer.cornerRadius = 14
        chatView.clipsToBounds = true
        
        chatButtomView.layer.cornerRadius = chatButtomView.frame.size.height / 2
        chatButtomView.layer.borderWidth = 1
        chatButtomView.layer.borderColor = UIColor(red: 49/255.0, green: 162/255.0, blue: 255/255.0, alpha: 1).cgColor
        chatButtomView.clipsToBounds = true
        
        sendTextButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        notificationWithInsertForUsers = false
        notificationListenerMessages()
        notificationListenerUsers()
        updateTopConstraint(delay: 0)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if firstTime
        {
            firstTime = false
            if Constants.messages.count > 1
            {
                self.messagesTableView.scrollToRow(at: IndexPath(row: Constants.messages.count - 1, section: 1), at: .bottom, animated: false)
            }
            updateTopConstraint(delay: 0)
            UIView.animate(withDuration: 0.3, animations: {
                self.messagesTableView.alpha = 1
                
            })
        }
        
        if shouldShowLoading
        {
            shouldShowLoading = false
            self.view.showLoadingIndicator()
            if let image = image
            {
                self.image = nil
                let controller = SendNotificationViewController.createViewController(type: 2, image: image, delegate: self, content: nil)
                self.present(controller, animated: false, completion: nil)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        removeNotificationListener()
    }
    
    func updateTopConstraint(delay: Double)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.messagesTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
        }
    }
    
    //MARK:- Create view controller
    static func createViewController() -> ChatViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        return controller
    }
    
    //MARK:- Actions
    @IBAction func back()
    {
        notificationBar.setLocationNotificationBar()
        Constants.unreadMessages = 0
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendText()
    {
        if sendImageButton.alpha == 1
        {
            plusButton()
        }
        if let contentText = contentMessageTextView.text, !contentText.isEmpty
        {
            let contentTextWithoutSpaces = contentText.split(separator: " ")
            if contentTextWithoutSpaces.count == 0
            {
                self.showErrors(title: "Content message error", bodyText: "Content message must contain at least one unblank character!", theme: .warning)
                return
            }
            else
            {
                var isBlank = true
                for content in contentTextWithoutSpaces
                {
                    let contentWithoutEndlines = content.split(separator: "\n")
                    if contentWithoutEndlines.count != 0
                    {
                        isBlank = false
                        break
                    }
                }
                if isBlank
                {
                    self.showErrors(title: "Content message error", bodyText: "Content message must contain at least one unblank character!", theme: .warning)
                    return
                }
                else
                {
                    self.contentMessageTextView.resignFirstResponder()
                    self.view.showLoadingIndicator()
                    if Constants.currentReachability
                    {
                        WebService.sharedInstance.sendTextMessage(content: contentMessageTextView.text) { (response) in
                            self.view.hideLoadingIndicator(animated: true)
                            guard response.isSuccess else {
                                self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to send the text message!", theme: .error)
                                return
                            }
                            self.contentMessageTextView.text = ""
                            
                            if let result = response.parsedResult as? [String:Any], let id = result["id"] as? Int64
                            {
                                self.getMessage(id: Int(id))
                            }
                        }
                    }
                }
            }
        }
        else
        {
            self.showErrors(title: "Content message missing", bodyText: "Content message can't be blank!", theme: .warning)
        }
    }
    
    @IBAction func plusButton()
    {
        if sendImageButton.alpha == 0
        {
            UIView.animate(withDuration: 0.2)
            {
                self.verticalPlusLine.transform = CGAffineTransform(rotationAngle: (90.0 * .pi) / 180.0)
                self.sendLocationVerticalConstraint.constant = 41
                self.sendImageHorizontalConstraint.constant = 41
                self.sendImageButton.alpha = 1
                self.sendImageImageView.alpha = 1
                self.sendLocationButton.alpha = 1
                self.sendLocationImageView.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
        else
        {
            UIView.animate(withDuration: 0.2)
            {
                self.verticalPlusLine.transform = CGAffineTransform(rotationAngle: (0 * .pi) / 180.0)
                self.sendLocationVerticalConstraint.constant = 0
                self.sendImageHorizontalConstraint.constant = 0
                self.sendImageButton.alpha = 0
                self.sendImageImageView.alpha = 0
                self.sendLocationButton.alpha = 0
                self.sendLocationImageView.alpha = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func sendImage()
    {
        contentMessageTextView.resignFirstResponder()
        mediaPicker.presentImagePicker(from: .both, media: .image, allowEditing: false)
    }
    
    @IBAction func sendLocation()
    {
        contentMessageTextView.resignFirstResponder()
        if let location = myLocation
        {
            self.view.showLoadingIndicator()
            let content = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
            generateLocationPreview(location: location) { (image) in
                if Constants.currentReachability
                {
                    let controller = SendNotificationViewController.createViewController(type: 3, image: image, delegate: self, content: content)
                    self.present(controller, animated: false, completion: nil)
                }
                else
                {
                    self.view.hideLoadingIndicator(animated: true)
                }
            }
        }
        else
        {
            self.showErrors(title: "Atention", bodyText: "Can't locate you!", theme: .warning)
        }
    }
    
    //MARK:- Functions
    @objc override func refreshMessages(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
            let messageID = infoNotification[0]
            if let messageIDInt = Int(messageID)
            {
                self.getMessage(id: messageIDInt)
            }
        }
        else if let messageID = notification.object as? String, let messageIDInt = Int(messageID)
        {
            self.getMessage(id: messageIDInt)
        }
    }
    
    func generateLocationPreview(location: CLLocation, completion: @escaping ((UIImage) -> ()))
    {
        let options = MKMapSnapshotter.Options()
        options.mapType = .hybrid
        options.region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 400, longitudinalMeters: 400)
        
        let snapshot = MKMapSnapshotter(options: options)
        
        snapshot.start { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                print("\(error?.localizedDescription ?? "Failed to create snapshot")")
                if let imagePlaceholder = UIImage(named: "placeholder")
                {
                    completion(imagePlaceholder)
                }
                return
            }
            
            UIGraphicsBeginImageContextWithOptions(options.size, true, 0)
            snapshot.image.draw(at: .zero)
            
            let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
            let pinImage = pinView.image
            
            var point = snapshot.point(for: location.coordinate)
            
            let pinCenterOffset = pinView.centerOffset
            point.x -= pinView.bounds.size.width / 2
            point.y -= pinView.bounds.size.height / 2
            point.x += pinCenterOffset.x
            point.y += pinCenterOffset.y
            pinImage?.draw(at: point)
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            
            UIGraphicsEndImageContext()
            if let completionImage = image
            {
                completion(completionImage)
            }
            else if let imagePlaceholder = UIImage(named: "placeholder")
            {
                completion(imagePlaceholder)
            }
        }
        
    }
    
    func getMessage(id: Int)
    {
        WebService.sharedInstance.getMessage(id: Int(id), completion: { (response) in
            self.view.hideLoadingIndicator(animated: true)
            guard response.isSuccess else {
                self.showErrors(title: "Attention", bodyText: response.errorMessage ?? "Failed to show new messages!", theme: .warning)
                return
            }
            let message = response.parsedResult as! Message
            if self.messagesTableView.contentOffset.y >= (self.messagesTableView.contentSize.height - self.messagesTableView.frame.size.height)
            {
                self.isScrolledToBottom = true
            }
            else
            {
                self.isScrolledToBottom = false
            }
            Constants.messages.append(message)
            self.messagesTableView.beginUpdates()
            self.messagesTableView.insertRows(at: [IndexPath(row: Constants.messages.count - 1, section: 1)], with: .fade)
            self.messagesTableView.endUpdates()
            self.updateTopConstraint(delay: 0)
            if message.sender?.deviceID == WebService.sharedInstance.currentUser?.deviceID
            {
                self.messagesTableView.scrollToRow(at: IndexPath(row: Constants.messages.count - 1, section: 1), at: .bottom, animated: false)
            }
            else if self.isScrolledToBottom
            {
                self.messagesTableView.scrollToRow(at: IndexPath(row: Constants.messages.count - 1, section: 1), at: .bottom, animated: false)
            }
        })
    }
    
    func hideTextView()
    {
        sendImageButton.isEnabled = false
        sendLocationButton.isEnabled = false
        sendTextButton.isEnabled = false
        contentMessageTextView.isSelectable = false
        contentMessageTextView.resignFirstResponder()
    }
    
    func showTextView()
    {
        sendImageButton.isEnabled = true
        sendLocationButton.isEnabled = true
        sendTextButton.isEnabled = true
        contentMessageTextView.isSelectable = true
    }
    
    override func showInternetConnectionError()
    {
        super.showInternetConnectionError()
        hideTextView()
    }
    
    override func hideInternetConnectionError()
    {
        super.hideInternetConnectionError()
        showTextView()
    }
    
}

extension ChatViewController: SendNotificationViewControllerDelegate
{
    func cancel()
    {
        self.view.hideLoadingIndicator(animated: false)
    }
    
    func send(type: Int, content: String, image: UIImage)
    {
        if type == 3
        {
            WebService.sharedInstance.sendCoordinatesMessage(content: content, image: image) { (response) in
                guard response.isSuccess else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to send your location!", theme: .error)
                    return
                }
                self.plusButton()
                if let result = response.parsedResult as? [String:Any], let id = result["id"] as? Int64
                {
                    self.getMessage(id: Int(id))
                }
                else
                {
                    self.view.hideLoadingIndicator(animated: true)
                }
            }
        }
        else if type == 2
        {
            WebService.sharedInstance.sendImageMessage(image: image) { (response) in
                guard response.isSuccess else {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to send the image!", theme: .error)
                    return
                }
                self.plusButton()
                if let result = response.parsedResult as? [String:Any], let id = result["id"] as? Int64
                {
                    self.getMessage(id: Int(id))
                }
            }
        }
        else
        {
            self.view.hideLoadingIndicator(animated: false)
        }
    }
    
    
}

extension ChatViewController: UITableViewDelegate
{
    //MARK:- Table view delegate functions
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard indexPath.section == 1 else { return }
        if let index = cellsHeightArray.firstIndex(where: {$0.row == indexPath.row})
        {
            cellsHeightArray[index].height = cell.frame.height
        }
        else
        {
            cellsHeightArray.append((indexPath.row, cell.frame.height))
        }
    }
}

//extension ChatViewController
//{
//    func scrollViewDidScroll(_ scrollView: UIScrollView)
//    {
//        let height = scrollView.frame.size.height
//        let contentYoffset = scrollView.contentOffset.y
//        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
//        if distanceFromBottom < height
//        {
//            self.isScrolledToBottom = true
//        }
//        else
//        {
//             self.isScrolledToBottom = false
//        }
//    }
//}

extension ChatViewController: UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        if textView.text.count != 0
        {
            sendTextButton.isEnabled = true
        }
        else
        {
            sendTextButton.isEnabled = false
        }
    }
}

// For UITableViewDataSource
extension ChatViewController
{
    //MARK:- Table view data source functions
    override func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == messagesTableView
        {
            return 2
        }
        else
        {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == messagesTableView
        {
            if section == 0
            {
                return 1
            }
            else
            {
                return Constants.messages.count
            }
        }
        else
        {
            return Constants.notifications.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if tableView == messagesTableView
        {
            if indexPath.section == 1
            {
                let message = Constants.messages[indexPath.row]
                if message.type == 1
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: kMessageTableViewCell, for: indexPath) as! MessageTableViewCell
                    let width = self.chatView.frame.size.width
                    cell.configure(for: message, width: width)
                    return cell
                }
                else
                {
                    let cell = tableView.dequeueReusableCell(withIdentifier: kImageTableViewCell, for: indexPath) as! ImageTableViewCell
                    let width = self.chatView.frame.size.width
                    cell.configure(for: message, width: width)
                    return cell
                }
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: kEmptyTableViewCell, for : indexPath) as! EmptyTableViewCell
                if tableContentHeight < tableView.superview!.frame.size.height - 16
                {
                    cell.heightConstraint.constant = tableView.frame.size.height - tableContentHeight
                }
                else
                {
                    cell.heightConstraint.constant = 16
                }
                return cell
            }
        }
        else
        {
            let notification = Constants.notifications[indexPath.row]
            if let notificationType = NotificationType.init(rawValue: notification.type)
            {
                switch notificationType {
                case .bleUserJoined, .bleUserConnected, .bleUserWarning, .bleUserLost, .bleUserDisconnected:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: Constants.imageByNotificationType(for: notification.type), content: notification.content)
                    return cell
                case .usernameChanged:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: UIImage(named: "ChangedName"), content: notification.content)
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: UIImage(named: "ChatMenu"), content: notification.content)
                    return cell
                }
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                return cell
            }
        }
    }
}

extension ChatViewController: MyMediaPickerDelegate
{
    func didCancel()
    {
        
    }
    
    func didFinishPickingMediaWithInfo(info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = (info[.originalImage] as? UIImage)?.fixedOrientation()
        {
            shouldShowLoading = true
            if Constants.currentReachability
            {
                self.image = image
            }
        }
    }
}
