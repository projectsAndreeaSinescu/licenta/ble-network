//
//  BluetoothViewController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 14/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import CoreBluetooth
import CoreLocation
import SwiftMessages

class BluetoothViewController: UIViewController {
    @IBOutlet weak var notificationBar: MyCorneredView!
    weak var internetConnectionView: MessageView!
    
    var bluetoothID: String?
    var connectionsDeviceID: [String] = []
    var centralManager:CBCentralManager? = nil
    var peripheralManager:CBPeripheralManager? = nil
    var timeAfterLastScan: Int = 0
    var timeAfterLastLocation: Int = 0
    var myDeviceID: String?
    
    var timer: Timer?
    
    let locationManager = CLLocationManager()
    
    var myLocation: CLLocation?
    
    var notificationWithInsertForMessages = true
    var notificationWithInsertForUsers = true
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
        self.peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        if let currentUser = WebService.sharedInstance.currentUser, let deviceID = currentUser.deviceID
        {
            self.myDeviceID = deviceID
        }

        if let currentUser = WebService.sharedInstance.currentUser, let bluetoothID = currentUser.bluetoothID
        {
            self.bluetoothID = bluetoothID
        }
        
        self.startTimer()
        
        self.locationManager.requestAlwaysAuthorization()
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled()
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeConnexion(notification:)), name: connexionDidChangeNotification, object: nil)
        
        if !WebService.sharedInstance.reachabilityManager.isReachable
        {
            showInternetConnectionError()
        }
        
        notificationBar.updateNotificationBar()
        notificationBar.moveNotificationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        timer?.invalidate()
    }

    //MARK:- Scan and stop scan functions for bluetooth
    
    func scanBLEDevices()
    {
        if let idBluetooth = bluetoothID
        {
            let SERVICE_UUID = CBUUID(string: idBluetooth)
            self.centralManager?.scanForPeripherals(withServices: [SERVICE_UUID], options: nil)
        }
        
    }
    
    func stopScanForBLEDevices()
    {
        centralManager?.stopScan()
        if WebService.sharedInstance.currentUser != nil && Constants.currentReachability
        {
            WebService.sharedInstance.updateConnections(location: myLocation, listNodes: connectionsDeviceID) { (response) in
                guard response.isSuccess else {
                    return
                }
            }
        }
    }
    
    //MARK:- Timer for scanning devices
    func startTimer()
    {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(findConnections), userInfo: nil, repeats: true)
    }
    
    @objc func findConnections()
    {
        if centralManager?.state == .poweredOn
        {
            timeAfterLastLocation = 0
            if timeAfterLastScan == 40
            {
                timeAfterLastScan = 0
                self.connectionsDeviceID = []
                self.scanBLEDevices()
            }
            else if timeAfterLastScan == 30
            {
                timeAfterLastScan = timeAfterLastScan + 1
                self.stopScanForBLEDevices()
            }
            else
            {
                timeAfterLastScan = timeAfterLastScan + 1
            }
        }
        else
        {
            timeAfterLastScan = 0
            if timeAfterLastLocation == 20
            {
                if myLocation == nil
                {
                    if WebService.sharedInstance.currentUser != nil && Constants.currentReachability
                    {
                        WebService.sharedInstance.updateConnections(location: myLocation, listNodes: []) { (response) in
                            self.timeAfterLastLocation = -1
                            guard response.isSuccess else {
                                //self.showAlert(title: "Atention", message: response.errorMessage!)
                                return
                            }
                            return
                        }
                    }
                }
            }
            else
            {
                if timeAfterLastLocation != -1
                {
                    timeAfterLastLocation = timeAfterLastLocation + 1
                }
            }
        }
    }
    
    //MARK:- Functions
    @objc func didChangeConnexion(notification: Notification)
    {
        guard let hasConnection = notification.object as? Bool else {
            return
        }
        
        Constants.currentReachability = hasConnection
        
        if !hasConnection
        {
            showInternetConnectionError()
        }
        else
        {
            hideInternetConnectionError()
        }
    }
    
    func showInternetConnectionError()
    {
        internetConnectionView = MessageView.viewFromNib(layout: .statusLine)
        internetConnectionView.configureTheme(.error)
        internetConnectionView.configureDropShadow()
        internetConnectionView.configureContent(body: "Please check your internet connection!")
        internetConnectionView.id = "internetConnectionView"
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.duration = .forever
        config.interactiveHide = false
        
        SwiftMessages.show(config: config, view: internetConnectionView)
    }
    
    func hideInternetConnectionError()
    {
        SwiftMessages.hide(id: "internetConnectionView")
        internetConnectionView = nil
    }

    func showErrors(title: String, bodyText: String, theme: Theme)
    {
        let errorView = MessageView.viewFromNib(layout: .cardView)
        errorView.configureTheme(theme)
        errorView.configureDropShadow()
        errorView.configureContent(title: title, body: bodyText)
        errorView.id = "errorView"
        errorView.button?.removeFromSuperview()
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.duration = .automatic
        config.interactiveHide = true
        
        SwiftMessages.show(config: config, view: errorView)
    }
}

extension BluetoothViewController: CBCentralManagerDelegate
{
    //MARK:- Central manager delegate functions
    func centralManagerDidUpdateState(_ central: CBCentralManager)
    {
        if central.state == .poweredOn
        {
            self.scanBLEDevices()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber)
    {
        guard let peripheralDeviceID = (advertisementData[CBAdvertisementDataLocalNameKey] as? String) else {
            return
        }
        
        if !connectionsDeviceID.contains(peripheralDeviceID)
        {
            connectionsDeviceID.append(peripheralDeviceID)
        }
    }
}

extension BluetoothViewController: CBPeripheralManagerDelegate
{
    //MARK:- Peripheral manager delegate functions
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager)
    {
        if peripheral.state == .poweredOn
        {
            if let bluetoothID = bluetoothID, let advertisementData = myDeviceID
            {
                let SERVICE_UUID = CBUUID(string: bluetoothID)
                peripheralManager!.startAdvertising([CBAdvertisementDataServiceUUIDsKey:[SERVICE_UUID], CBAdvertisementDataLocalNameKey: advertisementData])
            }
        }
        else
        {
            peripheralManager?.stopAdvertising()
        }
    }
}

extension BluetoothViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = manager.location else {
            return
        }
        if centralManager?.state != .poweredOn
        {
            if let lastLocation = myLocation, location.distance(from: lastLocation) >= 200
            {
                if WebService.sharedInstance.currentUser != nil && Constants.currentReachability
                {
                    WebService.sharedInstance.updateConnections(location: location, listNodes: []) { (response) in
                        guard response.isSuccess else {
                            return
                        }
                    }
                }
            }
        }
        self.myLocation = location
    }
}

extension BluetoothViewController
{
    //MARK:- Notification listner
    @objc func refreshUsers(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            if infoNotification[0] != WebService.sharedInstance.currentUser?.deviceID
            {
                if notificationWithInsertForUsers
                {
                    Constants.notifications.insert((infoNotification[2], infoNotification[1]), at: 0)
                }
                self.notificationBar.notificationTableView.beginUpdates()
                self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                self.notificationBar.notificationTableView.endUpdates()
                UIView.animate(withDuration: 0.5, animations: {
                    self.notificationBar.updateNotificationBar()
                }, completion: nil)
            }
        }
        else if let infoNotification = notification.object as? String
        {
            if notificationWithInsertForUsers
            {
                Constants.notifications.insert((NotificationType.usernameChanged.rawValue, infoNotification), at: 0)
            }
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
        }
    }
    
    @objc func refreshMessages(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            if notificationWithInsertForMessages
            {
                Constants.notifications.insert((infoNotification[2], infoNotification[1]), at: 0)
            }
            
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
        }
    }
    
    func notificationListenerUsers()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshUsers(notification:)), name: NSNotification.Name("refreshUsers"), object: nil)
    }
    
    func notificationListenerMessages()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(refreshMessages(notification:)), name: NSNotification.Name("newMessage"), object: nil)
    }
    
    func removeNotificationListener()
    {
        NotificationCenter.default.removeObserver(self)
    }
}

extension BluetoothViewController: UITableViewDataSource
{
    //MARK:- Table view data source functions
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return Constants.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let notification = Constants.notifications[indexPath.row]
        if let notificationType = NotificationType.init(rawValue: notification.type)
        {
            switch notificationType {
            case .bleUserJoined, .bleUserConnected, .bleUserWarning, .bleUserLost, .bleUserDisconnected:
                let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                cell.configure(image: Constants.imageByNotificationType(for: notification.type), content: notification.content)
                return cell
            case .usernameChanged:
                let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                cell.configure(image: UIImage(named: "ChangedName"), content: notification.content)
                return cell
            default:
                let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                cell.configure(image: UIImage(named: "ChatMenu"), content: notification.content)
                return cell
            }
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
            return cell
        }
    }
}
