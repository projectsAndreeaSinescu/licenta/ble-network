//
//  GroupController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 03/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import CoreBluetooth
import Pulsator

enum UserStatus: Int {
    case connecting = 0
    case connected = 1
    case warning = 2
    case lost = 3
    case disconnected = 4
}

class GroupViewController: BluetoothViewController {
    deinit
    {
        print("deinit ---- GroupViewController")
        self.removeNotificationListener()
    }
    
    var selectedIndexPath: IndexPath!
    var dashLines: [CAShapeLayer] = []
    
    @IBOutlet weak var neighboursRadarLabel: UILabel!
    @IBOutlet weak var usersTableView: UITableView!
    @IBOutlet weak var viewGroup: UIView!
    @IBOutlet weak var pulsatorView: UIView!
    @IBOutlet weak var radarView: UIView!
    @IBOutlet weak var centerRadarLabel: UILabel!
    @IBOutlet weak var centerRadarImageView: UIImageView!
    @IBOutlet weak var centerRadarButton: UIButton!
    @IBOutlet weak var northRadarLabel: UILabel!
    @IBOutlet weak var northRadarImageView: UIImageView!
    @IBOutlet weak var northRadarButton: UIButton!
    @IBOutlet weak var southRadarLabel: UILabel!
    @IBOutlet weak var southRadarImageView: UIImageView!
    @IBOutlet weak var southRadarButton: UIButton!
    @IBOutlet weak var westRadarLabel: UILabel!
    @IBOutlet weak var westRadarImageView: UIImageView!
    @IBOutlet weak var westRadarButton: UIButton!
    @IBOutlet weak var eastRadarLabel: UILabel!
    @IBOutlet weak var eastRadarImageView: UIImageView!
    @IBOutlet weak var eastRadarButton: UIButton!
    @IBOutlet weak var northWestRadarLabel: UILabel!
    @IBOutlet weak var northWestRadarImageView: UIImageView!
    @IBOutlet weak var northWestRadarButton: UIButton!
    @IBOutlet weak var southWestRadarLabel: UILabel!
    @IBOutlet weak var southWestRadarImageView: UIImageView!
    @IBOutlet weak var southWestRadarButton: UIButton!
    @IBOutlet weak var northEastRadarLabel: UILabel!
    @IBOutlet weak var northEastRadarImageView: UIImageView!
    @IBOutlet weak var northEastRadarButton: UIButton!
    @IBOutlet weak var southEastRadarLabel: UILabel!
    @IBOutlet weak var southEastRadarImageView: UIImageView!
    @IBOutlet weak var southEastRadarButton: UIButton!
    @IBOutlet weak var heightRadarConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentedControl: MySegmentedControl!
    
    //MARK:- View functions
    override func viewDidLoad()
    {
        super.viewDidLoad()

        segmentedControl.setWith(delegate: self, isList: true)
        
        pulsatorView.alpha = 0
        viewGroup.layer.cornerRadius = 14
        viewGroup.clipsToBounds = true

        sortUsers()
        self.usersTableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        notificationWithInsertForMessages = false
        notificationListenerMessages()
        notificationListenerUsers()
        usersTableView.layoutIfNeeded()
        usersTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        if radarView.frame.size.width < radarView.frame.size.height
        {
            heightRadarConstraint.constant = 4 * radarView.frame.size.width / 5
        }
        else
        {
            heightRadarConstraint.constant = 4 * radarView.frame.size.height / 5 
        }
        pulsatorRadar()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        removeNotificationListener()
    }
    
    //MARK:- Create view controller
    static func createViewController() -> GroupViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GroupViewController") as! GroupViewController
        return controller
    }
    
    //MARK:- Stop scan BLE
    override func stopScanForBLEDevices()
    {
        centralManager?.stopScan()
        if WebService.sharedInstance.currentUser != nil && Constants.currentReachability
        {
            WebService.sharedInstance.updateConnections(location: myLocation, listNodes: connectionsDeviceID) { (response) in
                guard response.isSuccess else {
//                    self.showAlert(title: "Atention", message: response.errorMessage!)
                    return
                }
                WebService.sharedInstance.getUsers { (response1) in
                    guard response1.isSuccess else {
//                        self.showAlert(title: "Atention", message: response1.errorMessage!)
                        return
                    }
                    Constants.users = response1.parsedResult as! [User]
                    self.sortUsers()
                    self.usersTableView.reloadData()
                    if self.radarView.alpha == 1
                    {
                        for user in Constants.users
                        {
                            if self.centerRadarLabel.text == user.username
                            {
                                self.loadRadar(for: user)
                                return
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Animations
    func switchListRadarAnimation()
    {
        UIView.animate(withDuration: 0.3)
        {
            self.usersTableView.alpha = 1 - self.usersTableView.alpha
            self.pulsatorView.alpha = 1 - self.pulsatorView.alpha
            self.radarView.alpha = 1 - self.radarView.alpha
        }
    }
    
    //MARK:- Functions
    @objc override func refreshUsers(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            if infoNotification[0] != WebService.sharedInstance.currentUser?.deviceID
            {
                self.notificationBar.notificationTableView.beginUpdates()
                self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                self.notificationBar.notificationTableView.endUpdates()
                UIView.animate(withDuration: 0.5, animations: {
                    self.notificationBar.updateNotificationBar()
                }, completion: nil)
            }
        }
        else if (notification.object as? String) != nil
        {
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
        }
        
        WebService.sharedInstance.getUsers { (response) in
            guard response.isSuccess else {
                //                    self.showAlert(title: "Atention", message: response.errorMessage!)
                return
            }
            Constants.users = response.parsedResult as! [User]
            self.sortUsers()
            self.usersTableView.reloadData()
            if self.radarView.alpha == 1
            {
                for user in Constants.users
                {
                    if self.centerRadarLabel.text == user.username
                    {
                        self.loadRadar(for: user)
                        return
                    }
                }
            }
        }
    }
    
    func pulsatorRadar()
    {
        let pulsator = Pulsator()
        pulsator.numPulse = 6
        pulsator.animationDuration = 7
        if radarView.frame.size.width < radarView.frame.size.height
        {
            pulsator.radius = radarView.frame.size.width / 2
        }
        else
        {
            pulsator.radius = radarView.frame.size.height / 2
        }
        pulsatorView.layer.addSublayer(pulsator)
        pulsator.start()
    }
    
    func addDashLine(from point: CGPoint, to destinationPoint: CGPoint)
    {
        let path = CGMutablePath()
        
        path.move(to: point)
        path.addLine(to: destinationPoint)
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path
        shapeLayer.strokeColor = northWestRadarLabel.textColor.cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [5,5]
        shapeLayer.fillColor = UIColor.clear.cgColor
        
        self.dashLines.append(shapeLayer)
        self.radarView.layer.addSublayer(shapeLayer)
    }
    
    func searchForUser(by username: String?) -> User?
    {
        for user in Constants.users
        {
            if user.username == username
            {
                return user
            }
        }
        return nil
    }
    
    static func userImage(for user: User) -> UIImage?
    {
        if let currentUser = WebService.sharedInstance.currentUser, user.username == currentUser.username, let userStatus = UserStatus(rawValue: Int(user.status)) {
            switch userStatus {
            case .connecting:
                return UIImage(named: "MeConnecting")
            case .warning:
                return UIImage(named: "MeWarning")
            case .lost:
                return UIImage(named: "MeUnavailable")
            case .disconnected:
                 return UIImage(named: "MeDisconnected")
            default:
                return UIImage(named: "Me")
            }
        }
        else if let userStatus = UserStatus(rawValue: Int(user.status)) {
            switch userStatus {
            case .connecting:
                return UIImage(named: "UserConnecting")
            case .warning:
                return UIImage(named: "UserWarning")
            case .lost:
                return UIImage(named: "UserUnavailable")
            case .disconnected:
                return UIImage(named: "UserDisconnected")
            default:
                return UIImage(named: "UserAvailable")
            }
        }
        else {
            return UIImage(named: "UserDisconnected")
        }
    }
    
    func loadRadar(for currentUser: User)
    {
        centerRadarImageView.image = GroupViewController.userImage(for: currentUser)
        centerRadarLabel.text = currentUser.username
        
        northRadarImageView.alpha = 0
        northRadarLabel.alpha = 0
        northRadarButton.alpha = 0
        northEastRadarImageView.alpha = 0
        northEastRadarLabel.alpha = 0
        northEastRadarButton.alpha = 0
        northWestRadarImageView.alpha = 0
        northWestRadarLabel.alpha = 0
        northWestRadarButton.alpha = 0
        southRadarImageView.alpha = 0
        southRadarLabel.alpha = 0
        southRadarButton.alpha = 0
        westRadarImageView.alpha = 0
        westRadarLabel.alpha = 0
        westRadarButton.alpha = 0
        eastRadarImageView.alpha = 0
        eastRadarLabel.alpha = 0
        eastRadarButton.alpha = 0
        southWestRadarImageView.alpha = 0
        southWestRadarLabel.alpha = 0
        southWestRadarButton.alpha = 0
        southEastRadarImageView.alpha = 0
        southEastRadarLabel.alpha = 0
        southEastRadarButton.alpha = 0
        
        for dashline in dashLines
        {
            self.radarView.layer.replaceSublayer(dashline, with: CALayer())
        }
        
        dashLines = []
        
        if let neighbours = currentUser.neighbours
        {
            var index = 0
            
            for neighbour in neighbours
            {
                let userNeighbour = neighbour as! User
                
                switch index {
                case 0:
                    northRadarLabel.text = userNeighbour.username
                    northRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    northRadarImageView.alpha = 1
                    northRadarLabel.alpha = 1
                    northRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarImageView.frame.origin.x + centerRadarImageView.frame.size.width / 2, y: centerRadarImageView.frame.origin.y - 2)
                    
                    let destinationPoint = CGPoint(x: northRadarLabel.frame.origin.x + northRadarLabel.frame.size.width / 2, y: northRadarLabel.frame.origin.y + northRadarLabel.frame.size.height)
                    
                    addDashLine(from: point, to: destinationPoint)
                case 1:
                    northEastRadarLabel.text = userNeighbour.username
                    northEastRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    northEastRadarImageView.alpha = 1
                    northEastRadarLabel.alpha = 1
                    northEastRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarImageView.frame.origin.x + centerRadarImageView.frame.size.width + 2, y: centerRadarImageView.frame.origin.y - 2)
                    
                    let destinationPoint = CGPoint(x: northEastRadarLabel.frame.origin.x + northRadarLabel.frame.size.width / 2, y: northEastRadarLabel.frame.origin.y + northEastRadarLabel.frame.size.height)
                    addDashLine(from: point, to: destinationPoint)
                case 2:
                    eastRadarLabel.text = userNeighbour.username
                    eastRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    eastRadarImageView.alpha = 1
                    eastRadarLabel.alpha = 1
                    eastRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarImageView.frame.origin.x + centerRadarImageView.frame.size.width + 4, y: centerRadarImageView.frame.origin.y + centerRadarImageView.frame.size.height - 2)
                    
                    let destinationPoint = CGPoint(x: eastRadarImageView.frame.origin.x - 2, y: eastRadarImageView.frame.origin.y + eastRadarImageView.frame.size.height / 2)
                    addDashLine(from: point, to: destinationPoint)
                case 3:
                    southEastRadarLabel.text = userNeighbour.username
                    southEastRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    southEastRadarImageView.alpha = 1
                    southEastRadarLabel.alpha = 1
                    southEastRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarLabel.frame.origin.x + centerRadarLabel.frame.size.width / 2 + 3, y: centerRadarLabel.frame.origin.y + centerRadarLabel.frame.size.height + 2)
                    
                    let destinationPoint = CGPoint(x: southEastRadarImageView.frame.origin.x - 2, y: southEastRadarImageView.frame.origin.y)
                    addDashLine(from: point, to: destinationPoint)
                case 4:
                    southRadarLabel.text = userNeighbour.username
                    southRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    southRadarImageView.alpha = 1
                    southRadarLabel.alpha = 1
                    southRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarLabel.frame.origin.x + centerRadarLabel.frame.size.width / 2, y: centerRadarLabel.frame.origin.y + centerRadarLabel.frame.size.height + 2)
                    
                    let destinationPoint = CGPoint(x: southRadarImageView.frame.origin.x + southRadarImageView.frame.size.width / 2, y: southRadarImageView.frame.origin.y - 2)
                    addDashLine(from: point, to: destinationPoint)
                case 5:
                    southWestRadarLabel.text = userNeighbour.username
                    southWestRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    southWestRadarImageView.alpha = 1
                    southWestRadarLabel.alpha = 1
                    southWestRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarLabel.frame.origin.x + centerRadarLabel.frame.size.width / 2 - 3, y: centerRadarLabel.frame.origin.y + centerRadarLabel.frame.size.height + 2)
                    
                    let destinationPoint = CGPoint(x: southWestRadarImageView.frame.origin.x + southWestRadarImageView.frame.size.width + 2, y: southWestRadarImageView.frame.origin.y)
                    addDashLine(from: point, to: destinationPoint)
                case 6:
                    westRadarLabel.text = userNeighbour.username
                    westRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    westRadarImageView.alpha = 1
                    westRadarLabel.alpha = 1
                    westRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarImageView.frame.origin.x - 4, y: centerRadarImageView.frame.origin.y + centerRadarImageView.frame.size.height - 2)
                    
                    let destinationPoint = CGPoint(x: westRadarImageView.frame.origin.x + westRadarImageView.frame.size.width + 2, y: westRadarImageView.frame.origin.y + westRadarImageView.frame.height / 2)
                    addDashLine(from: point, to: destinationPoint)
                case 7:
                    northWestRadarLabel.text = userNeighbour.username
                    northWestRadarImageView.image = GroupViewController.userImage(for: userNeighbour)
                    northWestRadarImageView.alpha = 1
                    northWestRadarLabel.alpha = 1
                    northWestRadarButton.alpha = 1
                    
                    self.view.layoutIfNeeded()
                    
                    let point = CGPoint(x: centerRadarImageView.frame.origin.x - 2, y: centerRadarImageView.frame.origin.y)
                    
                    let destinationPoint = CGPoint(x: northWestRadarLabel.frame.origin.x + northWestRadarLabel.frame.size.width / 2, y: northWestRadarLabel.frame.origin.y + northWestRadarLabel.frame.size.height + 2)
                    addDashLine(from: point, to: destinationPoint)
                default:
                    print("default")
                }
                index = index + 1
            }
            if index > 8
            {
                neighboursRadarLabel.text = "+\(index - 8) more neighbours"
            }
            else
            {
                neighboursRadarLabel.text = ""
            }
        }
    }
    
    func sortUsers()
    {
        Constants.users.sort(by: { (user, user1) -> Bool in
            if let status = UserStatus.init(rawValue: Int(user.status)), let status1 = UserStatus.init(rawValue: Int(user1.status)) {
                if status == .lost
                {
                    return true
                }
                else if status1 == .lost
                {
                    return false
                }
                else if status == .disconnected
                {
                    return false
                }
                else if status1 == .disconnected
                {
                    return true
                }
                else
                {
                    return user.status >= user1.status
                }
            }
            else {
                return true
            }
        })
    }
    
    //MARK:- Actions
    @IBAction func back()
    {
        notificationBar.setLocationNotificationBar()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func northNeighbour()
    {
        let username = northRadarLabel.text
        for user in Constants.users {
            if username == user.username
            {
                loadRadar(for: user)
                return
            }
        }
    }
    
    @IBAction func southNeighbour()
    {
        let username = southRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func westNeighbour()
    {
        let username = westRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func eastNeighbour()
    {
        let username = eastRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func northWestNeighbour()
    {
        let username = northWestRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func northEastNeighbour()
    {
        let username = northEastRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func southWestNeighbour()
    {
        let username = southWestRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func southEastNeighbour()
    {
        let username = southEastRadarLabel.text
        if let user = searchForUser(by: username)
        {
            loadRadar(for: user)
        }
    }
    
    @IBAction func centerRadar()
    {
        let numberOfRows = Constants.users.count
        for row in 0...numberOfRows-1
        {
            if centerRadarLabel.text == Constants.users[row].username
            {
                let indexPath = IndexPath(row: row, section: 0)
                if let selected = selectedIndexPath
                {
                    if selected.row != indexPath.row
                    {
                        usersTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                        tableView(usersTableView, didSelectRowAt: indexPath)
                    }
                    else
                    {
                        usersTableView.scrollToRow(at: indexPath, at: .none, animated: true)
                    }
                }
                else
                {
                    usersTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                    tableView(usersTableView, didSelectRowAt: indexPath)
                }
                segmentedControl.animateTo(isList: true)
                switchListRadarAnimation()
                return
            }
        }
    }
}

extension GroupViewController: UITableViewDelegate
{
    //MARK:- Table view delegate functions
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath) as? UserTableViewCell {
            if cell.lastNeighbourView == nil && cell.usernameLabel.text != WebService.sharedInstance.currentUser?.username
            {
                return
            }
        }
        if let oldIndexPath = selectedIndexPath
        {
            if oldIndexPath == indexPath
            {
                selectedIndexPath = nil
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            else
            {
                selectedIndexPath = indexPath
                tableView.reloadRows(at: [oldIndexPath, indexPath], with: .automatic)
            }
        }
        else
        {
            selectedIndexPath = indexPath
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        if let selectedIndexPath = selectedIndexPath , let visibleIndexPaths = tableView.indexPathsForVisibleRows
        {
            if visibleIndexPaths.first == selectedIndexPath
            {
                self.usersTableView.scrollToRow(at: selectedIndexPath, at: .top, animated: true)
            }
            else if visibleIndexPaths.last == selectedIndexPath
            {
                self.usersTableView.scrollToRow(at: selectedIndexPath, at: .bottom, animated: true)
            }
        }
    }
}

// For UITableViewDataSource
extension GroupViewController
{
    //MARK:- Table view data source functions
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == usersTableView
        {
            return Constants.users.count
        }
        else
        {
            return Constants.notifications.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == usersTableView
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: kUserTableViewCell, for: indexPath) as! UserTableViewCell
            let user = Constants.users[indexPath.row]
            cell.configure(for: user, delegate: self)
            cell.isLittle = selectedIndexPath != indexPath
            return cell
        }
        else
        {
            let notification = Constants.notifications[indexPath.row]
            if let notificationType = NotificationType.init(rawValue: notification.type)
            {
                switch notificationType {
                case .bleUserJoined, .bleUserConnected, .bleUserWarning, .bleUserLost, .bleUserDisconnected:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: Constants.imageByNotificationType(for: notification.type), content: notification.content)
                    return cell
                case .usernameChanged:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: UIImage(named: "ChangedName"), content: notification.content)
                    return cell
                default:
                    let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                    cell.configure(image: UIImage(named: "ChatMenu"), content: notification.content)
                    return cell
                }
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: kNotificationTableViewCell, for: indexPath) as! NotificationTableViewCell
                return cell
            }
        }
    }
}

extension GroupViewController: MySegmentedControlDelegate
{
    func didToggle(isList: Bool)
    {
        if isList && usersTableView.alpha == 0
        {
            switchListRadarAnimation()
        }
        else if !isList && usersTableView.alpha == 1, let currentUser = WebService.sharedInstance.currentUser
        {
            loadRadar(for: currentUser)
            switchListRadarAnimation()
        }
    }
}

