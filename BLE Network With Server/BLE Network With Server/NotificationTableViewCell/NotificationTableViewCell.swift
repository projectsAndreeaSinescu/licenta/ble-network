//
//  NotificationTableViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 29/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kNotificationTableViewCell = "NotificationTableViewCell"

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var notificationImageView: UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    func configure(image: UIImage?, content: String)
    {
        contentLabel.text = content
        notificationImageView.image = image
    }
}
