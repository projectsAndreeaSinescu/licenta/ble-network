//
//  UserTableViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 03/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kUserTableViewCell = "UserTableViewCell"

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var circleImage: UIImageView!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var haveEdgeLabel: UILabel!
    @IBOutlet weak var haveEdgeImage: UIImageView!
    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var neighboursView: UIView!
    @IBOutlet weak var bubblesContainerHeight: NSLayoutConstraint!
    
    var listOfSubviews: [UIView] = []
    
    var lastNeighbourView: UIView!
    
    weak var delegate: GroupViewController?
    var user: User?
    
    var isLittle: Bool! {
        didSet
        {
            bubblesContainerHeight.constant = isLittle ? 0 : (lastNeighbourView != nil ?  lastNeighbourView.frame.origin.y + lastNeighbourView.frame.height + 4 : 0)
            
            downButton.setImage(isLittle ? UIImage(named: "ArrowDown") : UIImage(named: "ArrowUp"), for: .normal)
            
            if lastNeighbourView != nil
            {
                neighboursView.updateConstraints()
            }
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: false)
    }
    
    func configure(for user: User, delegate: GroupViewController)
    {
        self.user = user
        self.delegate = delegate
        usernameLabel.text = user.username
        
        for subview in listOfSubviews
        {
            subview.removeFromSuperview()
        }
        
        circleImage.image = GroupViewController.userImage(for: user)
        
        listOfSubviews = []
        
        lastNeighbourView = nil
        
        if let neighbours = user.neighbours
        {
            for neighbour in neighbours
            {
                let neighbourUser = neighbour as! User
                let neighbourView = NeighbourCell.createView(for: neighbourUser)
                if let lastNeighbourView = lastNeighbourView
                {
                    if lastNeighbourView.frame.origin.x + lastNeighbourView.frame.width + neighbourView.frame.width <= neighboursView.frame.width
                    {
                        neighbourView.frame.origin = CGPoint(x: lastNeighbourView.frame.origin.x + lastNeighbourView.frame.width + 6, y: lastNeighbourView.frame.origin.y)
                        neighboursView.addSubview(neighbourView)
                    }
                    else
                    {
                        neighbourView.frame.origin = CGPoint(x: 0, y: lastNeighbourView.frame.origin.y + lastNeighbourView.frame.height)
                        
                        neighboursView.addSubview(neighbourView)
                    }
                }
                else
                {
                    neighbourView.frame.origin = CGPoint(x: 0, y: 0)
                    
                    neighboursView.addSubview(neighbourView)
                }
                lastNeighbourView = neighbourView
                listOfSubviews.append(neighbourView)
            }
        }
        if lastNeighbourView == nil
        {
            downButton.alpha = 0
            haveEdgeImage.alpha = 0
            haveEdgeLabel.alpha = 0
            detailsButton.alpha = 0
        }
        else
        {
            downButton.alpha = 1
            haveEdgeImage.alpha = 1
            haveEdgeLabel.alpha = 1
            detailsButton.alpha = 1
        }
    }
    
    @IBAction func details()
    {
        delegate?.segmentedControl.animateTo(isList: false)
        if let user = user
        {
            delegate?.loadRadar(for: user)
        }
        delegate?.switchListRadarAnimation()
    }
}
