//
//  NeighbourCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 03/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class NeighbourCell: UIView {
    @IBOutlet weak var usernameLabel: UILabel!
    
    static func createView(for neighbour: User) -> NeighbourCell
    {
        let view = Bundle.main.loadNibNamed("NeighbourCell", owner: nil, options: nil)![0] as! NeighbourCell
        if let username = neighbour.username
        {
            let textSize = username.size(withAttributes: [NSAttributedString.Key.font: view.usernameLabel.font])
            
            var cellSize = textSize
            cellSize.width += 20
            cellSize.height += 8
            
            view.frame.size = cellSize
            
            view.usernameLabel.text = username
            view.usernameLabel.layer.cornerRadius = textSize.height / 2
            view.usernameLabel.clipsToBounds = true
        }
        return view
    }

}
