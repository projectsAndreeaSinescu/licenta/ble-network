//
//  WebServiceResponse.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

enum WebServiceResponseCode : Int {
    case Success = 0
    case ResponseStructureError = -100
    case NoConnectivity = -200
    case NoUser = -300
    case InvalidToken = -400
    
}

public class WebServiceResponse: NSObject {
    var responseData : [String : Any]?
    var errorMessage : String?
    var errorCode : Int = WebServiceResponseCode.Success.rawValue
    var parsedResult : Any?
    
    init(_ dictionary: [String : Any])
    {
        super.init()
        responseData = dictionary
        setErrorFromDict(dictionary)
    }
    
    init(urlResponse: URLResponse?)
    {
        super.init()
        guard let response = urlResponse as? HTTPURLResponse else {
            self.errorMessage = "Unknown error"
            self.errorCode = WebServiceResponseCode.ResponseStructureError.rawValue
            return
        }
        
        self.errorMessage = "Server error"
        self.errorCode = response.statusCode
    }
    
    
    init(error: String, code : WebServiceResponseCode = WebServiceResponseCode.ResponseStructureError)
    {
        super.init()
        self.errorMessage = error
        self.errorCode = code.rawValue
    }
    
    init(code : WebServiceResponseCode)
    {
        super.init()
        self.errorCode = code.rawValue
        if code == .NoConnectivity
        {
            self.errorMessage = "Please check your internet connection"
        }
        else if code == .NoUser
        {
            self.errorMessage = "No current user"
        }
        else if code == .ResponseStructureError
        {
            self.errorMessage = "Unknown Error"
        }
        else if code == .InvalidToken
        {
            self.errorMessage = "Invalid Token"
        }
    }
    
    private func setErrorFromDict(_ dictionary: [String : Any])
    {
        guard let responseCode = dictionary["error_code"] as? [String : Any] else {
            errorCode = WebServiceResponseCode.ResponseStructureError.rawValue
            errorMessage = "Unknown Error"
            return
        }
        
        guard let message = responseCode["message"] as? String else {
            errorCode = WebServiceResponseCode.ResponseStructureError.rawValue
            errorMessage = "Unknown Error"
            return
        }
        
        guard let code = responseCode["code"] else {
            errorCode = WebServiceResponseCode.ResponseStructureError.rawValue
            errorMessage = "Unknown Error"
            return
        }
        
        if (Int(Utils.getDouble(obj: code as AnyObject)) == WebServiceResponseCode.Success.rawValue)
        {
            errorCode = WebServiceResponseCode.Success.rawValue
            errorMessage = nil
        }
        else
        {
            errorMessage = message
            errorCode = Int(Utils.getDouble(obj: code as AnyObject))
        }
    }
    
    var isSuccess : Bool {
        get
        {
            return errorCode == WebServiceResponseCode.Success.rawValue
        }
        
    }
}
