//
//  WebService.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import AFNetworking
import Firebase
import CoreLocation

let connexionDidChangeNotification = Notification.Name.init(rawValue: "connexionDidChangeNotification")

public class WebService: AFHTTPSessionManager {
    public typealias CompletionBlock = (_ completion: WebServiceResponse) -> Void
    public typealias ProgressBlock = (_: Float) -> Void
    
    public static var sharedInstance = WebService()
    
    static var myBaseURL: String {
        get {
            //return "http://192.168.65.84:3000"
            return "https://licenta.hypersense-software.com"
        }
    }
    
    static var apiURI: String = "/api/v1"
    var extensionsData = [String: Any]()
    
    public init()
    {
        let url = NSURL(string: "\(WebService.myBaseURL)\(WebService.apiURI)") as URL?
        super.init(baseURL: url, sessionConfiguration: nil)
        requestSerializer = AFJSONRequestSerializer()
        
        self.securityPolicy = AFSecurityPolicy(pinningMode: .none)
        self.securityPolicy.allowInvalidCertificates = true
        self.securityPolicy.validatesDomainName = false
        
        self.reachabilityManager.startMonitoring()
        self.reachabilityManager.setReachabilityStatusChange { (status) in
            self.reachabilityUpdatedStatus(status)
        }
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Reachability
    var isReachableOrUndetermined: Bool {
        if self.reachabilityManager.isReachable {
            return true
        } else if (self.reachabilityManager.networkReachabilityStatus == .unknown) {
            return true
        } else {
            return false
        }
    }
    
    func reachabilityUpdatedStatus(_ state: AFNetworkReachabilityStatus)
    {
        NotificationCenter.default.post(name: connexionDidChangeNotification, object: reachabilityManager.isReachable)
        
        if self.reachabilityManager.isReachable
        {
            restartSync()
        }
    }
    
    func restartSync() {
        guard self.currentUser != nil else {
            return
        }
    }
    
    //MARK:- Authentification
    var accessToken: String?
    var accessTokenExpiration: Date?
    var currentUser: User?
    var url: String?
    
    var hasValidAccessToken: Bool {
        if accessToken == nil || accessTokenExpiration == nil
        {
            return false
        }
        else
        {
            return accessTokenExpiration!.timeIntervalSinceNow > 0
        }
    }
    
    func checkAccessTokenAndContinue(completion: @escaping CompletionBlock)
    {
        guard reachabilityManager.isReachable else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        guard currentUser != nil else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoUser)
            completion(response)
            return
        }
        
        if self.hasValidAccessToken
        {
            let response = WebServiceResponse(code: WebServiceResponseCode.Success)
            response.parsedResult = self.accessToken
            completion(response)
            return
        }
    }
    
    var settingsLoaded = false
    
    public func apiSettings(completion: @escaping CompletionBlock)
    {
        guard !settingsLoaded else {
            let response = WebServiceResponse(code: WebServiceResponseCode.Success)
            completion(response)
            return
        }
        
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        let generalErrorResponse = "Failed to connect to server"
        let address = "clientusers/apiSettings"
        
        self.get(address, parameters: nil, progress: nil, success: { (task, result) in
            guard let resultDict = result as? [String : Any] else {
                let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                completion(response)
                return
            }
            
            guard let responseDict = resultDict["response"] as? [String : Any] else {
                LogManager.staticInstance.writeToLogs("apiSettings result: \(resultDict)")
                let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                response.errorMessage = generalErrorResponse
                completion(response)
                return
            }
            
            if let fileStorage = responseDict["fileStorage"] as? [String : Any]
            {
                if let url = fileStorage["url"] as? String
                {
                    self.url = url
                    self.settingsLoaded = true
                    let response = WebServiceResponse(code: WebServiceResponseCode.Success)
                    completion(response)
                }
            }
            
        }) { (task, error) in
            debugPrint(error)
            let response = WebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
    }
    
    public func login(deviceID: String, username: String, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        let params = ["deviceID": deviceID, "username" : username]
        let generalErrorResponse = "Failed to login"
        let address = "clientusers/clientLogin"
        
        self.post(address, parameters: params, progress: nil, success: { (task, result) in
            guard let resultDict = result as? [String : Any] else {
                let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                completion(response)
                return
            }
            
            let response = self.parseLoginResponse(resultDict)
            guard response.isSuccess else {
                LogManager.staticInstance.writeToLogs("Login result: \(resultDict)")
                completion(response)
                return
            }
            completion(response)
            
        }) { (task, error) in
            debugPrint(error)
            let response = WebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
        
    }
    
    public func logout(completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        guard self.accessToken != nil else {
            self.cleanUserData()
            let response = WebServiceResponse(code: WebServiceResponseCode.Success)
            completion(response)
            return
        }
        
        let generalErrorResponse = "Failed to log out"
        let address = "clientusers/clientLogout?access_token=\(self.accessToken!)"
        let params = ["deviceID": currentUser!.deviceID, "username":currentUser!.username]

        Messaging.messaging().unsubscribe(fromTopic: currentUser!.topic!)

        self.post(address, parameters: params, progress: nil, success: { (task, result) in
            self.cleanUserData()
            
            guard let resultDict = result as? [String : Any] else {
                let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                response.errorMessage = generalErrorResponse
                completion(response)
                return
            }
            
            let response = WebServiceResponse(resultDict)
            guard response.isSuccess else {
                LogManager.staticInstance.writeToLogs("error: \(generalErrorResponse), for result: \(resultDict)")
                completion(response)
                return
            }
            WebService.sharedInstance.currentUser = nil
            completion(response)
        }) { (task, error) in
            debugPrint(error)
            let response = WebServiceResponse(urlResponse: task?.response)
            response.errorMessage = generalErrorResponse
            completion(response)
        }
    }
    
    func parseLoginResponse(_ resultDict: [String : Any]) -> WebServiceResponse
    {
        let generalErrorResponse = "Login failed"
        let response = WebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? [String : Any] else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        guard let accessToken = responseDict["id"] as? String else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        guard var userDict = responseDict["user"] as? [String : Any] else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        guard let tokenCreatedString = responseDict["created"] as? String else {
            LogManager.staticInstance.writeToLogs("Token Created string is missing: \(responseDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        guard responseDict["ttl"] != nil else {
            LogManager.staticInstance.writeToLogs("Token TTL string is missing: \(responseDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
            
        }
        
        let ttl = Utils.getDouble(obj: responseDict["ttl"] as AnyObject)
        
        guard let tokenCreatedDate = MyDateFormatter.getDate(dateString: tokenCreatedString,
                                                             dateFormat: MyDateFormatter.KnownFormat.ISODateTime.rawValue,
                                                             timeZoneFormat: MyDateFormatter.KnownTimezones.UTC.rawValue) else {
                                                                
                                                                LogManager.staticInstance.writeToLogs("Token Created date string is missing: \(responseDict)")
                                                                let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                                                                response.errorMessage = generalErrorResponse
                                                                return response
        }
        
        
        self.accessTokenExpiration = tokenCreatedDate.addingTimeInterval(ttl)
        self.accessToken = accessToken
        
        guard let bluetoothID = responseDict["bluetoothID"] as? String else {
            LogManager.staticInstance.writeToLogs("BluetoothID string is missing: \(responseDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        guard let firebaseTopic = responseDict["firebaseTopic"] as? String else {
            LogManager.staticInstance.writeToLogs("firebaseTopic string is missing: \(responseDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        userDict["bluetoothID"] = bluetoothID
        userDict["firebaseTopic"] = firebaseTopic
        
        Messaging.messaging().subscribe(toTopic: firebaseTopic)
        
        let user = CoreDataController.sharedInstance.createOrReplaceUser(userDict)
        self.currentUser = user
        self.restartSync()
        
        response.parsedResult = user
        return response
    }
    
    func cleanUserData()
    {
        self.accessToken = nil
    }
    
}

extension WebService
{
    //MARK:- Users
    public func updateUserName(username: String, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            let generalErrorResponse = "Failed to update username"
            let address = "clientusers/clientUpdateUserName?access_token=\(self.accessToken!)"
            let params = ["username": username]
            
            self.post(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = WebServiceResponse(resultDict)
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                guard var userDict = resultDict["response"] as? [String:Any] else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = resultDict
                userDict["bluetoothID"] = self.currentUser?.bluetoothID
                userDict["firebaseTopic"] = self.currentUser?.firebaseTopic
                let user = CoreDataController.sharedInstance.createOrReplaceUser(userDict)
                self.currentUser = user
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
    
    public func getUsers(completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            let generalErrorResponse = "Failed to get users"
            let address = "clientusers/findUsers?access_token=\(self.accessToken!)"
            let params = ["limit": Double(0), "page": Double(0)]
            
            self.get(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                
                let response = self.parseGetUsersResponse(resultDict)
                
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("Get users result: \(resultDict)")
                    completion(response)
                    return
                }
                
                completion(response)
            }, failure: { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            })
        }
    }
    
    func parseGetUsersResponse(_ resultDict: [String : Any]) -> WebServiceResponse
    {
        let generalErrorResponse = "Failed to get users"
        let response = WebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? NSArray else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        var users:[User] = []
        
        for userDict in responseDict
        {
            if let dict = userDict as? [String : Any] {
                let newUser = CoreDataController.sharedInstance.createOrReplaceUser(dict)
                users.append(newUser)
            }
        }
        response.parsedResult = users
        return response
    }
    
    //MARK:- Messsages
    public func sendTextMessage(content: String, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            var params = [:] as [String : Any]
            
            params["content"] = content
            
            let generalErrorResponse = "Failed to send message"
            let address = "messages/createMessage?access_token=\(self.accessToken!)"
            
            self.post(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = WebServiceResponse(resultDict)
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = resultDict["response"]
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
    
    public func sendImageMessage(image: UIImage, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            var params = [:] as [String : Any]
            
            guard let imageJpegData = image.jpegData(compressionQuality: 0.5) else
            {
                completion(result)
                return
            }
            
            let aspectRatio = (image.size.height > 0) ? (image.size.width / image.size.height) : 1
            let generalErrorResponse = "Failed to send image"
            let address = "messages/createImageMessage?access_token=\(self.accessToken!)"
            params["storageData"] = String.init(data: try! JSONSerialization.data(withJSONObject: ["aspectRatio":aspectRatio],
                                                                             options: .prettyPrinted)
                , encoding: .utf8)
            
            self.post(address, parameters: params, constructingBodyWith: { (data) in
                data.appendPart(withFileData: imageJpegData, name: "file", fileName: "image.jpeg", mimeType: "image/jpeg")
                
            }, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = WebServiceResponse(resultDict)
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = resultDict["response"]
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
    
    public func sendCoordinatesMessage(content: String, image: UIImage, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            var params = [:] as [String : Any]
            
            guard let imageJpegData = image.jpegData(compressionQuality: 0.5) else {
                completion(result)
                return
            }
            
            let generalErrorResponse = "Failed to send coordinates"
            let address = "messages/createCoordinatesMessage?access_token=\(self.accessToken!)"
            
            params["storageData"] = String.init(data: try! JSONSerialization.data(withJSONObject: ["content":content],
                                                                                  options: .prettyPrinted)
                , encoding: .utf8)
            
            self.post(address, parameters: params, constructingBodyWith: { (data) in
                data.appendPart(withFileData: imageJpegData, name: "file", fileName: "image.jpeg", mimeType: "image/jpeg")
                
            }, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = WebServiceResponse(resultDict)
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = resultDict["response"]
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
    
    public func getMessages(completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            let generalErrorResponse = "Failed to get messages"
            let address = "messages/getMessages?access_token=\(self.accessToken!)"
            let params = ["createdAt": 0, "limit": Double(0), "page": Double(0)]
            
            self.get(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                
                let response = self.parseGetMessagesResponse(resultDict)
                
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("Get messages result: \(resultDict)")
                    completion(response)
                    return
                }
                
                completion(response)
            }, failure: { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            })
        }
    }
    
    func parseGetMessagesResponse(_ resultDict: [String : Any]) -> WebServiceResponse
    {
        let generalErrorResponse = "Failed to get messages"
        let response = WebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? NSArray else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        var messages:[Message] = []
        
        for message in responseDict
        {
            if let dict = message as? [String : Any] {
                let newMessage = CoreDataController.sharedInstance.createOrReplaceMessage(dict)
                messages.append(newMessage)
            }
        }
        response.parsedResult = messages
        return response
    }
    
    public func getMessage(id: Int, completion: @escaping CompletionBlock)
    {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            let generalErrorResponse = "Failed to get message"
            let address = "messages/getMessage?access_token=\(self.accessToken!)"
            let params = ["id": id]
            
            self.get(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                
                let response = self.parseGetMessageResponse(resultDict)
                
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("Get messages result: \(resultDict)")
                    completion(response)
                    return
                }
                
                completion(response)
            }, failure: { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            })
        }
    }
    
    func parseGetMessageResponse(_ resultDict: [String : Any]) -> WebServiceResponse
    {
        let generalErrorResponse = "Failed to get message"
        let response = WebServiceResponse(resultDict)
        guard response.isSuccess else {
            return response
        }
        
        guard let responseDict = resultDict["response"] as? [String : Any] else {
            LogManager.staticInstance.writeToLogs("login result: \(resultDict)")
            let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
            response.errorMessage = generalErrorResponse
            return response
        }
        
        let newMessage = CoreDataController.sharedInstance.createOrReplaceMessage(responseDict)
            
        response.parsedResult = newMessage
        return response
    }
    
    //MARK:- Connections
    public func updateConnections(location: CLLocation?, listNodes: [String], completion: @escaping CompletionBlock) {
        guard self.isReachableOrUndetermined else {
            let response = WebServiceResponse(code: WebServiceResponseCode.NoConnectivity)
            completion(response)
            return
        }
        
        self.checkAccessTokenAndContinue { (result) in
            guard result.isSuccess else {
                completion(result)
                return
            }
            var params = ["neighbours":listNodes.joined(separator: ",")] as [String : Any]
            if let myLocation = location
            {
                params["latitude"] = myLocation.coordinate.latitude
                params["longitude"] = myLocation.coordinate.longitude
            }
            let generalErrorResponse = "Failed to update neighbours"
            let address = "clientusers/clientUpdateNeighbours?access_token=\(self.accessToken!)"
            
            self.post(address, parameters: params, progress: nil, success: { (task, result) in
                guard let resultDict = result as? [String : Any] else {
                    let response = WebServiceResponse(code: WebServiceResponseCode.ResponseStructureError)
                    completion(response)
                    return
                }
                let response = WebServiceResponse(resultDict)
                guard response.isSuccess else {
                    LogManager.staticInstance.writeToLogs("\(generalErrorResponse) result: \(resultDict)")
                    completion(response)
                    return
                }
                response.parsedResult = listNodes.joined(separator: ",")
                completion(response)
            }) { (task, error) in
                debugPrint(error)
                let response = WebServiceResponse(urlResponse: task?.response)
                response.errorMessage = generalErrorResponse
                completion(response)
            }
        }
    }
}
