//
//  MessageTableViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 14/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kMessageTableViewCell = "MessageTableViewCell"

class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var myContentLabel: UILabel!
    @IBOutlet weak var myContentMessageView: UIView!
    @IBOutlet weak var contentMessageView: UIView!
    @IBOutlet weak var emptyCellView: UIView!
    @IBOutlet weak var widthContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthMyContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightEmptyConstraint: NSLayoutConstraint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    func configure(for message: Message, width: CGFloat)
    {
        usernameLabel.text = ""
        contentLabel.text = ""
        myContentLabel.text = ""
        
        let username = message.sender?.username
        if let contentMessage = message.content
        {
            let contentSize = contentMessage.size(withAttributes: [NSAttributedString.Key.font: contentLabel.font])
            if let currentUsername = username, currentUsername != WebService.sharedInstance.currentUser?.username
            {
                let usernameSize = currentUsername.size(withAttributes: [NSAttributedString.Key.font: usernameLabel.font])
                if usernameSize.width > contentSize.width
                {
                    if usernameSize.width >= 3 * width / 4 - 35
                    {
                        widthContentConstraint.constant = 3 * width / 4 + 5
                    }
                    else
                    {
                        widthContentConstraint.constant = usernameSize.width + 35
                    }
                }
                else
                {
                    if contentSize.width >= 3 * width / 4 - 35
                    {
                        widthContentConstraint.constant = 3 * width / 4 + 5
                    }
                    else
                    {
                        widthContentConstraint.constant = contentSize.width + 35
                    }
                }
                myContentMessageView.alpha = 0
                contentMessageView.alpha = 1
                usernameLabel.text = username
                contentLabel.text = message.content
                contentMessageView.layer.borderWidth = 1
                contentMessageView.layer.borderColor = UIColor(red: 49/255.0, green: 162/255.0, blue: 255/255.0, alpha: 1).cgColor
                contentMessageView.layer.cornerRadius = 22
                contentMessageView.clipsToBounds = true
            }
            else
            {
                if contentSize.width > 3 * width / 4 - 35
                {
                    widthMyContentConstraint.constant = 3 * width / 4 + 5
                }
                else
                {
                    widthMyContentConstraint.constant = contentSize.width + 35
                }
                myContentMessageView.alpha = 1
                contentMessageView.alpha = 0
                myContentLabel.text = message.content
                myContentMessageView.layer.borderWidth = 1
                myContentMessageView.layer.borderColor = UIColor.white.cgColor
                myContentMessageView.layer.cornerRadius = 22
                myContentMessageView.clipsToBounds = true
            }
        }
    }
}
