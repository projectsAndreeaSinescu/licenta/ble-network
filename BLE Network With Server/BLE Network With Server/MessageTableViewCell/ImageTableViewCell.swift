//
//  ImageTableViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 15/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit

let kImageTableViewCell = "ImageTableViewCell"

class ImageTableViewCell: UITableViewCell {
    @IBOutlet weak var myContentMessageView: UIView!
    @IBOutlet weak var contentMessageView: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var myMessageImageView: UIImageView!
    @IBOutlet weak var widthContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var widthMyContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightImageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var myMapButton: UIButton!
    
    var location: CLLocation!

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    func configure(for message: Message, width: CGFloat)
    {
        usernameLabel.text = nil
        messageImageView?.image = nil
        myMessageImageView?.image = nil
        mapButton.alpha = 0
        myMapButton.alpha = 0
        
        let username = message.sender?.username
        
        if username != WebService.sharedInstance.currentUser?.username
        {
            usernameLabel.text = username
            widthContentConstraint.constant = 3 * width / 4
            if message.type == 2
            {
                if let data = message.image
                {
                    self.messageImageView.image = UIImage(data: data)
                }
                else
                {
                    if let contentMessage = message.content, let url = URL(string: "https://s3-eu-west-1.amazonaws.com/andreea-development/" + contentMessage)
                    {
                        self.messageImageView.sd_setImage(with: url) { (image, error, cacheType, url) in
                            if let myImage = image
                            {
                                message.image = myImage.pngData()
                                CoreDataController.sharedInstance.saveContext()
                            }
                        }
                    }
                }
                if message.imageAspectRatio > 0
                {
                    heightImageViewConstraint.constant = 3 * width / 4 / CGFloat(message.imageAspectRatio)
                }
                else
                {
                    heightImageViewConstraint.constant = 200
                }
            }
            else if message.type ==  3
            {
                heightImageViewConstraint.constant = 3 * width / 4
                
                if let contentMessage = message.content
                {
                    let coordinates = contentMessage.split(separator: ",")
                    if let latitude = Double(coordinates[0]), let longitude = Double(coordinates[1])
                    {
                        let location = CLLocation(latitude: latitude, longitude: longitude)
                        
                        let urlImage = coordinates[2]
                        
                        self.location = location

                        if let data = message.image
                        {
                            self.messageImageView.image = UIImage(data: data)
                        }
                        else
                        {
                            if let url = URL(string: "https://s3-eu-west-1.amazonaws.com/andreea-development/" + urlImage)
                            {
                                self.messageImageView.sd_setImage(with: url) { (image, error, cacheType, url) in
                                    if let myImage = image
                                    {
                                        message.image = myImage.pngData()
                                        CoreDataController.sharedInstance.saveContext()
                                    }
                                }
                            }
                        }
                        mapButton.alpha = 1
                    }
                }
            }
            myContentMessageView.alpha = 0
            contentMessageView.alpha = 1
            contentMessageView.layer.borderWidth = 1
            contentMessageView.layer.borderColor = UIColor(red: 49/255.0, green: 162/255.0, blue: 255/255.0, alpha: 1).cgColor
            messageImageView.layer.cornerRadius = 22
            messageImageView.clipsToBounds = true
            contentMessageView.layer.cornerRadius = 22
            contentMessageView.clipsToBounds = true
        }
        else
        {
            widthMyContentConstraint.constant = 3 * width / 4
            if message.type == 2
            {
                if let data = message.image
                {
                    self.myMessageImageView.image = UIImage(data: data)
                }
                else
                {
                    if let contentMessage = message.content, let url = URL(string: "https://s3-eu-west-1.amazonaws.com/andreea-development/" + contentMessage)
                    {
                        self.myMessageImageView.sd_setImage(with: url) { (image, error, cacheType, url) in
                            if let myImage = image {
                                message.image = myImage.pngData()
                                CoreDataController.sharedInstance.saveContext()
                            }
                        }
                    }
                }
                if message.imageAspectRatio > 0
                {
                    heightImageViewConstraint.constant = 3 * width / 4 / CGFloat(message.imageAspectRatio)
                }
                else
                {
                    heightImageViewConstraint.constant = 200
                }
            }
            else if message.type == 3
            {
                heightImageViewConstraint.constant = 3 * width / 4
                
                if let contentMessage = message.content
                {
                    let coordinates = contentMessage.split(separator: ",")
                    if let latitude = Double(coordinates[0]), let longitude = Double(coordinates[1])
                    {
                        let location = CLLocation(latitude: latitude, longitude: longitude)
                        
                        let urlImage = coordinates[2]
                        
                        self.location = location
                        
                        if let data = message.image
                        {
                            self.myMessageImageView.image = UIImage(data: data)
                        }
                        else
                        {
                            if let url = URL(string: "https://s3-eu-west-1.amazonaws.com/andreea-development/" + urlImage)
                            {
                                self.myMessageImageView.sd_setImage(with: url) { (image, error, cacheType, url) in
                                    if let myImage = image
                                    {
                                        message.image = myImage.pngData()
                                        CoreDataController.sharedInstance.saveContext()
                                    }
                                }
                            }
                        }
                        myMapButton.alpha = 1
                    }
                }
            }
            myContentMessageView.alpha = 1
            contentMessageView.alpha = 0
            myMessageImageView.layer.cornerRadius = 22
            myMessageImageView.clipsToBounds = true
            myContentMessageView.layer.borderWidth = 1
            myContentMessageView.layer.borderColor = UIColor.white.cgColor
            myContentMessageView.layer.cornerRadius = 22
            myContentMessageView.clipsToBounds = true
        }
    }
    
    //MARK:- Actions
    @IBAction func goToMap()
    {
        let link = URL(string: "http://maps.apple.com/?address=\(location.coordinate.latitude),\(location.coordinate.longitude)")
        if let url = link
        {
            UIApplication.shared.open(url)
        }
    }
}

extension UIImage
{
    func fixedOrientation() -> UIImage?
    {
        guard imageOrientation != .up else {
            //This is default orientation, don't need to do anything
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            //CGImage is not available
            return nil
        }
        
        guard let colorSpace = cgImage.colorSpace, let ctx = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: cgImage.bitsPerComponent, bytesPerRow: 0, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil //Not able to create CGContext
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat.pi)
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat.pi / 2.0)
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat.pi / -2.0)
            break
        case .up, .upMirrored:
            break
        }
        
        //Flip image one more time if needed to, this is to prevent flipped image
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        guard let newCGImage = ctx.makeImage() else {
            return nil
        }
        return UIImage.init(cgImage: newCGImage, scale: 1, orientation: .up)
    }
}

