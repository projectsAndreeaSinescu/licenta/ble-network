//
//  EmptyTableViewCell.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 07/06/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kEmptyTableViewCell = "EmptyTableViewCell"

class EmptyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
