//
//  MapController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 22/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: BluetoothViewController, MKMapViewDelegate {
    deinit
    {
        print("deinit ---- MapViewController")
        self.removeNotificationListener()
    }
    
    @IBOutlet weak var myMapView: MKMapView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var pinView: UIView!
    
    var usersAnnotations: [(annotation: MKPointAnnotation, locationCoordinate: CLLocationCoordinate2D)] = []
    var firstTime = true

    //MARK:- View functions
    override func viewDidLoad()
    {
        super.viewDidLoad()
        myMapView.layer.cornerRadius = 14
        myMapView.clipsToBounds = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        self.loadMap()
        UIView.animate(withDuration: 1)
        {
            self.myMapView.alpha = 1
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        myMapView.alpha = 0
        notificationWithInsertForMessages = false
        notificationListenerMessages()
        notificationListenerUsers()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
        removeNotificationListener()
    }

    
    //MARK:- Create view controller
    static func createViewController() -> MapViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        return controller
    }
    
    //MARK:- Actions
    @IBAction func back()
    {
        for userAnnotation in usersAnnotations
        {
            myMapView.removeAnnotation(userAnnotation.annotation)
        }
        notificationBar.setLocationNotificationBar()
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Map View Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        guard annotation is MKPointAnnotation else {
            return nil
        }
        
        
        usernameLabel.text = annotation.title as? String
        pinView.layoutIfNeeded()
        let image = pinView.asImage()
        
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
        view.image = image
        view.canShowCallout = true
        
        let locationButton = UIButton(type: UIButton.ButtonType.custom) as UIButton
        locationButton.frame.size.width = 44
        locationButton.frame.size.height = 44
        locationButton.backgroundColor = UIColor.clear
        locationButton.setBackgroundImage(UIImage(named: "MapMenu"), for: .normal)
        
        view.leftCalloutAccessoryView = locationButton
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl)
    {
        if let annotation = view.annotation as? MKPointAnnotation
        {
            for userAnnotation in usersAnnotations
            {
                if userAnnotation.annotation == annotation
                {
                    let link = URL(string: "http://maps.apple.com/?address=\(userAnnotation.locationCoordinate.latitude),\(userAnnotation.locationCoordinate.longitude)")
                    if let url = link
                    {
                        UIApplication.shared.open(url)
                    }
                }
            }
        }
    }
    
    //MARK:- Functions
    @objc override func refreshUsers(notification: Notification)
    {
        if let infoNotification = notification.object as? [String], infoNotification.count == 3
        {
            if infoNotification[0] != WebService.sharedInstance.currentUser?.deviceID
            {
                self.notificationBar.notificationTableView.beginUpdates()
                self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                self.notificationBar.notificationTableView.endUpdates()
                UIView.animate(withDuration: 0.5, animations: {
                    self.notificationBar.updateNotificationBar()
                }, completion: nil)
            }
        }
        else if (notification.object as? String) != nil
        {
            self.notificationBar.notificationTableView.beginUpdates()
            self.notificationBar.notificationTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
            self.notificationBar.notificationTableView.endUpdates()
            UIView.animate(withDuration: 0.5, animations: {
                self.notificationBar.updateNotificationBar()
            }, completion: nil)
        }
        
        WebService.sharedInstance.getUsers { (response) in
            guard response.isSuccess else {
                //                    self.showErrors(title: "Atention", bodyText: response.errorMessage ?? "Failed to get users!", theme: .error)
                //                    self.showAlert(title: "Atention", message: response.errorMessage!)
                return
            }
            Constants.users = response.parsedResult as! [User]
            for userAnnotation in self.usersAnnotations
            {
                self.myMapView.removeAnnotation(userAnnotation.annotation)
            }
            self.loadMap()
        }
    }
    
    func loadMap()
    {
        var currentUserCoordinate: CLLocationCoordinate2D?
        var firstUserCoordinate: CLLocationCoordinate2D?
        var firstUser = true
        for user in Constants.users
        {
            if let location = user.location
            {
                let coordinate = location.split(separator: ",")
                let userPin = MKPointAnnotation()
                if let latitude = Double(coordinate[0]), let longitude = Double(coordinate[1])
                {
                    userPin.coordinate = CLLocation(latitude: latitude, longitude: longitude).coordinate
                    if WebService.sharedInstance.currentUser?.username == user.username
                    {
                        currentUserCoordinate = userPin.coordinate
                    }
                    if firstUser
                    {
                        firstUserCoordinate = userPin.coordinate
                        firstUser = false
                    }
                    userPin.title = user.username
                    usersAnnotations.append((userPin, userPin.coordinate))
                    myMapView.addAnnotation(userPin)
                }
            }
        }
        if firstTime
        {
            firstTime = false
            if let currentCoordinate = currentUserCoordinate
            {
                let region = MKCoordinateRegion(center: currentCoordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
                myMapView.setRegion(region, animated: true)
            }
            else if let firstCoordinate = firstUserCoordinate
            {
                let region = MKCoordinateRegion(center: firstCoordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
                myMapView.setRegion(region, animated: true)
            }
            else
            {
                firstTime = true
                self.showErrors(title: "Atention", bodyText: "No user has location!", theme: .warning)
            }
        }
    }
}

extension UIView
{
    func asImage() -> UIImage
    {
        if #available(iOS 10.0, *)
        {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        }
        else
        {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
