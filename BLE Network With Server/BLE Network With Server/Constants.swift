//
//  Constants.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 05/06/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    static var joinedNotify = UserDefaults.standard.bool(forKey: "joinedNotify")
    static var connectedNotify = UserDefaults.standard.bool(forKey: "connectedNotify")
    static var warningNotify = UserDefaults.standard.bool(forKey: "warningNotify")
    static var lostNotify = UserDefaults.standard.bool(forKey: "lostNotify")
    static var disconnectedNotify = UserDefaults.standard.bool(forKey: "disconnectedNotify")
    static var messageNotify = UserDefaults.standard.bool(forKey: "messageNotify")
    static var usernameChangedNotify = UserDefaults.standard.bool(forKey: "usernameChangedNotify")
    static var currentReachability: Bool = true
    static var users: [User] = []
    static var messages: [Message] = []
    
    //For notification bar
    static var currentOrigin: CGPoint!
    static var currentTopLeftCornerRadius: CGFloat!
    static var currentTopRightCornerRadius: CGFloat!
    static var currentBottomLeftCornerRadius: CGFloat!
    static var currentBottomRightCornerRadius: CGFloat!
    static var visibility: (Int, Int, Int)!
    static var notifications: [(type:String, content:String)] = []
    static var unreadMessages: Int = 0
    
    static func imageByNotificationType(for status: String) -> UIImage?
    {
        if let status = NotificationType.init(rawValue: status)
        {
            switch status {
            case .bleUserJoined:
                return UIImage(named: "UserConnecting")
            case .bleUserWarning:
                return UIImage(named: "UserWarning")
            case .bleUserLost:
                return UIImage(named: "UserUnavailable")
            case .bleUserDisconnected:
                return UIImage(named: "UserDisconnected")
            case .textMessage:
                return UIImage(named: "ChatMenu")
            case .coordinatesMessage:
                return UIImage(named: "MapMenu")
            case .imageMessage:
                //revino
                return UIImage(named: "MapMenu")
            default:
                return UIImage(named: "UserAvailable")
            }
        }
        else
        {
            return UIImage(named: "UserDisconnected")
        }
    }
}
