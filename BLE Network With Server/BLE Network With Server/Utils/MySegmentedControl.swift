//
//  MySegmentedControl.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 14/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

protocol MySegmentedControlDelegate: class {
    func didToggle(isList: Bool)
}

class MySegmentedControl: UIView {
    @IBOutlet private weak var leftConstraint: NSLayoutConstraint!
    
    private var leftValue: CGFloat = 0
    private weak var delegate: MySegmentedControlDelegate!
    
    func setWith(delegate: MySegmentedControlDelegate, isList: Bool)
    {
        self.delegate = delegate
        self.leftValue = isList ? 0 : self.frame.width / 2
    }
    
    func animateTo(isList: Bool)
    {
        UIView.animate(withDuration: 0.3)
        {
            self.leftConstraint.constant = isList ? 0 : self.frame.width / 2
            self.layoutIfNeeded()
        }
    }
    
    private func toggle(isList: Bool)
    {
        delegate.didToggle(isList: isList)
        animateTo(isList: isList)
    }
    
    @IBAction private func list()
    {
        toggle(isList: true)
    }
    
    @IBAction private func radar()
    {
        toggle(isList: false)
    }
    
    @IBAction private func didPan(gesture: UIPanGestureRecognizer)
    {
        let translation = gesture.translation(in: self.superview)
        
        if gesture.state == .began
        {
            leftValue = leftConstraint.constant
        }
        else if gesture.state == .changed
        {
            leftConstraint.constant = max(0, min(leftValue + translation.x, self.frame.width / 2))
        }
        else
        {
            toggle(isList: leftConstraint.constant < self.frame.width / 4)
        }
    }
}
