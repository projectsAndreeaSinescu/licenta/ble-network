//
//  MyLoadingIndicator.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 14/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class MyLoadingIndicator: UIView {
    static func createView(frame: CGRect) -> MyLoadingIndicator
    {
        let view = Bundle.main.loadNibNamed("MyLoadingIndicator", owner: nil, options: nil)![0] as! MyLoadingIndicator
        view.frame = frame
        
        return view
    }
    
    @IBOutlet private weak var radarImageView: UIImageView!
    
    func show()
    {
        startRotating()
        UIView.animate(withDuration: 0.2)
        {
            self.alpha = 1
        }
    }
    
    func hide(completion: @escaping (() -> ()))
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 0
        }) { (_) in
            self.stopRotating()
            completion()
        }
    }
    
    private func startRotating()
    {
        let kAnimationKey = "rotation"
        
        if self.radarImageView.layer.animation(forKey: kAnimationKey) == nil {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = 1.5
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float.pi * 2
            self.radarImageView.layer.add(animate, forKey: kAnimationKey)
        }
    }
    
    private func stopRotating()
    {
        let kAnimationKey = "rotation"
        
        if self.radarImageView.layer.animation(forKey: kAnimationKey) != nil
        {
            self.radarImageView.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
}

extension UIView
{
    func showLoadingIndicator()
    {
        let  loadingIndicator = MyLoadingIndicator.createView(frame: self.frame)
        loadingIndicator.alpha = 0
        self.addSubview(loadingIndicator)
        loadingIndicator.show()
    }
    
    func hideLoadingIndicator(animated: Bool = true)
    {
        if let loadingIndicator = self.subviews.first(where: {$0 as? MyLoadingIndicator != nil}) as? MyLoadingIndicator
        {
            if animated
            {
                loadingIndicator.hide
                {
                    loadingIndicator.removeFromSuperview()
                }
            }
            else
            {
                loadingIndicator.removeFromSuperview()
            }
        }
    }
}
