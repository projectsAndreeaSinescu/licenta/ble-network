//
//  MyMediaPicker.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 15/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

enum MySourceType {
    case camera
    case photoLibrary
    case both
}

enum MyMediaType: String {
    case video = "video"
    case image = "image"
    case both = "media"
}

protocol MyMediaPickerDelegate: class {
    func didCancel()
    func didFinishPickingMediaWithInfo(info: [UIImagePickerController.InfoKey : Any])
}

/// Use media pricker to present an image picker
/// Make sure the object doesn't get deinitializate before it answers
class MyMediaPicker: NSObject, UINavigationControllerDelegate {
    private weak var delegate: MyMediaPickerDelegate!
    private var alertMedia: MyMediaType!
    private var alertAllowEditing = false
    private var allowEditing = false
    private weak var controller: UIViewController!
    
    init(controller:UIViewController, delegate: MyMediaPickerDelegate)
    {
        self.delegate = delegate
        self.controller = controller
    }
    
    
    /// Present an media picker for source "library" or "camera".
    /// Present an alert controller for both souces.
    ///
    ///
    /// - Parameters:
    ///   - controller: view controller that will present the picker
    ///   - delegate: implement the delegate methods to be notified when user canceled, selected image or selected video
    ///   - source: the source from which media should be picked, default is CAMERA
    ///   - media: default is IMAGE, video is with sound
    ///   - allowEditing: allow user to edit media, default is FALSE
    func presentImagePicker(from source: MySourceType = .camera, media: MyMediaType = .image, allowEditing: Bool = false)
    {
        self.allowEditing = allowEditing
        switch source {
        case .camera, .photoLibrary:
            self.chooseFrom(source: source, media: media, allowEditing: allowEditing)
            
        case .both:
            self.chooseFromBoth(source: source, media: media, allowEditing: allowEditing)
        }
    }
    
    
    private func chooseFrom(source: MySourceType, media: MyMediaType, allowEditing: Bool)
    {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = source == .camera ? .camera : .photoLibrary
        
        switch media {
        case .image:
            imagePicker.mediaTypes = [kUTTypeImage as String]
            
        case .video:
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            
        case .both:
            imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        }
        
        imagePicker.allowsEditing = allowEditing
        controller.present(imagePicker, animated: true, completion: nil)
    }
    
    private func chooseFromBoth(source: MySourceType,  media: MyMediaType, allowEditing: Bool)
    {
        let alert = UIAlertController(title: "Add Image", message: "Please pick your input source:", preferredStyle: .actionSheet)
        let actionCamera = UIAlertAction(title: "Camera", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                self.chooseFrom(source: .camera, media: media, allowEditing: allowEditing)
            }
        }
        let actionGallery = UIAlertAction(title: "Gallery", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
            {
                self.chooseFrom(source: .photoLibrary, media: media, allowEditing: allowEditing)
            }
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(actionCamera)
        alert.addAction(actionGallery)
        alert.addAction(actionCancel)
        
        self.alertMedia = media
        self.alertAllowEditing = allowEditing
        
        controller.present(alert, animated: true, completion: nil)
    }
}

extension MyMediaPicker: UIImagePickerControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
        guard delegate != nil else {
            return
        }
        delegate.didCancel()
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        picker.dismiss(animated: true, completion: nil)
        
        guard delegate != nil else {
            return
        }
        delegate.didFinishPickingMediaWithInfo(info: info)
    }
}
