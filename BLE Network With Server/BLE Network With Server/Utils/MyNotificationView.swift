//
//  MyNotificationView.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 28/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class MyNotificationView: UIView {
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationContentLabel: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBAction func removeNotification()
    {
        hide(delay: 0)
    }
    
    func hide(delay: Double)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay)
        {
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = -self.frame.height
                self.superview!.layoutIfNeeded()
            }
        }
    }

}
