//
//  Utils.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

let kPrintLogs = true

class Utils: NSObject {
    static func getDouble(obj: AnyObject) -> Double
    {
        if obj as? Double != nil
        {
            return obj as! Double
        }
        else if obj as? String != nil
        {
            return Double(obj as! String)!
        }
        
        return Double(0)
    }
    
    static func getNumber(obj: AnyObject) -> NSNumber
    {
        if obj as? NSNumber != nil
        {
            return obj as! NSNumber
        }
        else
        {
            return NSNumber(value: getDouble(obj: obj))
        }
    }
    
    //MARK: File management
    static func documentsDirectory() -> URL
    {
        return FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory
            , in: FileManager.SearchPathDomainMask.userDomainMask).last!
    }
    
    static func documentsDirectory(path : String) -> URL
    {
        return Utils.documentsDirectory().appendingPathComponent(path)
    }
}

