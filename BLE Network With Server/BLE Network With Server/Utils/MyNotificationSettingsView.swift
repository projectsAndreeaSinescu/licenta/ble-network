//
//  MyNotificationSettingsView.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 27/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

protocol MyNotificationSettingsDelegate: class {
    func didToggle(settingsView: MyNotificationSettingsView)
}

class MyNotificationSettingsView: UIView {
    @IBOutlet private weak var greyCircleView: UIView!
    @IBOutlet private weak var blueView: UIView!
    @IBOutlet private weak var checkViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textLabel: UILabel!
    @IBOutlet private weak var selectedTextLabel: UILabel!
    @IBOutlet private weak var selectedTextWidthConstraint: NSLayoutConstraint!
    
    weak private var delegate: MyNotificationSettingsDelegate!
    var settingActivated = false
    
    @IBAction private func toggleSetting()
    {
        settingActivated = !settingActivated
        
        UIView.animate(withDuration: 0.3)
        {
            self.greyCircleView.alpha = self.settingActivated ? 0 : 1
            self.blueView.alpha = self.settingActivated ? 1 : 0
            self.checkViewWidthConstraint.constant = self.settingActivated ? 30 : 0
            self.selectedTextWidthConstraint.constant = self.settingActivated ? self.textLabel.frame.width : 0
            self.layoutIfNeeded()
        }
        
        if delegate != nil
        {
            delegate.didToggle(settingsView: self)
        }
    }
    
    func set(settingActivated: Bool)
    {
        self.layoutIfNeeded()
        self.settingActivated = settingActivated
        blueView.alpha = settingActivated ? 1 : 0
        checkViewWidthConstraint.constant = settingActivated ? 30 : 0
        selectedTextWidthConstraint.constant = settingActivated ? textLabel.frame.width : 0
        greyCircleView.alpha = self.settingActivated ? 0 : 1
        greyCircleView.layer.borderColor = UIColor(red: 168/255.0, green: 169/255.0, blue: 181/255.0, alpha: 1).cgColor
        greyCircleView.layer.borderWidth = 1
    }
    
}
