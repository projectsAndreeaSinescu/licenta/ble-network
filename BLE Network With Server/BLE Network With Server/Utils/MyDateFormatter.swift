//
//  MyDateFormatter.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import Foundation
class MyDateFormatter {
    enum KnownFormat : String {
        case USShorDateFormat = "MM/dd/yyyy"
        case ISODateTime = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
    }
    
    enum KnownTimezones : String {
        case UTC = "UTC"
    }
    
    static func getDate(dateString: String, dateFormat : String, timeZoneFormat : String? = nil) -> Date!
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        if (timeZoneFormat != nil)
        {
            if let timeZone = TimeZone(abbreviation: timeZoneFormat!)
            {
                dateFormatter.timeZone = timeZone
            }
            else
            {
                return nil
            }
        }
        
        let date = dateFormatter.date(from: dateString)
        return date
    }
    
}
