//
//  MyTextFieldView.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 07/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

/**
 Inherits UITextfield
 Functionality:
 - add textfield underline
 - text left, right insets
 - min character validation
 - max character validation - textfield will not allow user to exceed the limit
 - if the keyboard is of type email - text vaidation includes basic email validation
 - settable from storyboard and xib
 */
//@IBDesignable
class MyTextField: UITextField {
    private var underlineLayer: CALayer?
    
    /// disable copy, paste and select all
    @IBInspectable var disablaActions: Bool = false
    
    ///border color
    @IBInspectable var myBorderColor : UIColor?
    {
        didSet
        {
            self.layer.borderColor = myBorderColor?.cgColor
        }
        
    }
    
    ///border Width
    @IBInspectable var myBorderWidth : Int = 0
    {
        didSet
        {
            self.layer.borderWidth = CGFloat(myBorderWidth)
        }
        
    }
    
    ///border corner
    @IBInspectable var myBorderCornerRadius : Int = 0
    {
        didSet
        {
            self.layer.cornerRadius = CGFloat(myBorderCornerRadius)
        }
    }
    ///underline height
    @IBInspectable var myUnderlineHeight : Int = 2
    ///textinset left and right
    @IBInspectable var myTextHorizontalEdge : Int = 2
    ///maximum number of characters
    @IBInspectable var myMaxCharacterCount : Int = 0
    ///minimum number of characters
    @IBInspectable var myMinCharacterCount : Int = 0
    ///underline coulor
    @IBInspectable var underlineColor : UIColor?
    {
        didSet
        {
            if underlineLayer == nil
            {
                underlineLayer = CALayer()
                underlineLayer?.frame = CGRect(x: CGFloat(0),
                                               y: self.frame.size.height - CGFloat(myUnderlineHeight),
                                               width:  self.frame.size.width,
                                               height: CGFloat(myUnderlineHeight))
                
                self.layer.addSublayer(underlineLayer!)
                self.layer.masksToBounds = true
                
            }
            underlineLayer?.backgroundColor = underlineColor?.cgColor
        }
    }
    
    ///placeholder color
    @IBInspectable var placeholderColor : UIColor?
    {
        didSet
        {
        }
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        addTarget(self, action: #selector(limitEditingChanged), for: .editingChanged)
        if (self.placeholder != nil) && (self.font != nil)
        {
            if placeholderColor != nil
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: self.font!.pointSize), NSAttributedString.Key.foregroundColor : placeholderColor!])
            }
            else
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: self.font!.pointSize)])
            }
        }
    }
    
    ///update underline when layout changes
    override func layoutSubviews()
    {
        if underlineLayer != nil
        {
            underlineLayer?.frame.size.width = self.frame.size.width
            underlineLayer?.frame.origin.y = self.frame.size.height - CGFloat(myUnderlineHeight)
        }
        
        super.layoutSubviews()
        
    }
    
    ///add left and right insets
    override func textRect(forBounds bounds: CGRect) -> CGRect
    {
        var insetBounds = bounds
        insetBounds.origin.x = insetBounds.origin.x + CGFloat(myTextHorizontalEdge)
        insetBounds.size.width = insetBounds.size.width - CGFloat(myTextHorizontalEdge)
        return insetBounds
    }
    
    
    ///add left and right insets
    override func editingRect(forBounds bounds: CGRect) -> CGRect
    {
        var insetBounds = bounds
        insetBounds.origin.x = insetBounds.origin.x + CGFloat(myTextHorizontalEdge)
        insetBounds.size.width = insetBounds.size.width - CGFloat(myTextHorizontalEdge)
        return insetBounds
    }
    
    ///limit the number of characters - only if a max character cout value is set and is above 0
    @objc func limitEditingChanged()
    {
        if let text = self.text
        {
            if (self.myMaxCharacterCount > 0)
            {
                if (text.count > myMaxCharacterCount)
                {
                    let index = text.index(text.startIndex, offsetBy: myMaxCharacterCount)
                    self.text = String(text[..<index])
                    
                }
                
            }
        }
        self.validFieldValue(true)
    }
    
    /**
     Validate the current value
     - if character limit is set - check the limit
     - if invalid: placeholder and underline become red, otherwise original colour is used
     - returns: true if all checks are passed
     */
    func validateFieldValue() -> Bool
    {
        guard  myMinCharacterCount > 0 else
        {
            return true
        }
        
        if let text = self.text
        {
            if text.count >= myMinCharacterCount
            {
                self.validFieldValue(true)
                return true
            }
        }
        
        self.validFieldValue(false)
        return false
    }
    
    ///field is invalid, use in case the field has a combined validation condition(Ex: repeat password)
    func invalidFieldValue()
    {
        self.validFieldValue(false)
    }
    
    /**
     Update UI based on validation
     - if invalid: placeholder and underline become red, otherwise original colour is used
     - parameter valid: text content validation value updates UI configuration
     */
    private func validFieldValue(_ valid: Bool)
    {
        if valid
        {
            if (underlineLayer != nil)
            {
                underlineLayer?.backgroundColor = underlineColor?.cgColor
            }
            else
            {
                self.layer.borderColor = self.myBorderColor?.cgColor
            }
            
            if (self.placeholder != nil) && (self.font != nil)
            {
                if placeholderColor != nil
                {
                    self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                    attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: self.font!.pointSize), NSAttributedString.Key.foregroundColor : placeholderColor!])
                }
                else
                {
                    self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                    attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: self.font!.pointSize)])
                }
            }
        }
        else
        {
            if (underlineLayer != nil)
            {
                underlineLayer?.backgroundColor = UIColor.red.cgColor
            }
            else
            {
                self.layer.borderColor = UIColor.red.cgColor
            }
            if (self.placeholder != nil) && (self.font != nil)
            {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                                attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: self.font!.pointSize),
                                                                             NSAttributedString.Key.foregroundColor: UIColor.red])
            }
        }
    }
    
    /// disable paste and other actions
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool
    {
        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(paste(_:)) {
            
            return disablaActions == false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}
