//
//  MyCorneredView.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 29/05/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit

class MyCorneredView: UIView {
    @IBOutlet weak var topLeftView: UIView!
    @IBOutlet weak var topRightView: UIView!
    @IBOutlet weak var bottomLeftView: UIView!
    @IBOutlet weak var bottomRightView: UIView!
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var noNotificationImageView: UIImageView!
    @IBOutlet weak var sleepNotificationImageView: UIImageView!
    @IBOutlet weak var goToNotificationsButton: UIButton!

    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var animationNotificationView: UIView!
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var didPanView: UIView!
    
    var panGesture: UIPanGestureRecognizer!
    var initialOrigin: CGPoint!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(gesture:)))
        self.addGestureRecognizer(panGesture)
        setInitialState()
    }

    func setInitialState()
    {
        topLeftView.layer.cornerRadius = self.frame.width / 2
        topLeftView.layer.maskedCorners = .layerMinXMinYCorner
        topLeftView.layer.borderColor = UIColor.gray.cgColor
        topLeftView.layer.borderWidth = 0.5
        
        topRightView.layer.cornerRadius = 0//self.frame.width / 2
        topRightView.layer.maskedCorners = .layerMaxXMinYCorner
        topRightView.layer.borderColor = UIColor.gray.cgColor
        topRightView.layer.borderWidth = 0.5
        
        bottomLeftView.layer.cornerRadius = self.frame.width / 2
        bottomLeftView.layer.maskedCorners = .layerMinXMaxYCorner
        bottomLeftView.layer.borderColor = UIColor.gray.cgColor
        bottomLeftView.layer.borderWidth = 0.5
        
        bottomRightView.layer.cornerRadius = 0//self.frame.width / 2
        bottomRightView.layer.maskedCorners = .layerMaxXMaxYCorner
        bottomRightView.layer.borderColor = UIColor.gray.cgColor
        bottomRightView.layer.borderWidth = 0.5
    }
    
    //MARK:- Set Location of Notification bar
    func setLocationNotificationBar()
    {
        var visibility = (1, 0, 0)
        
        if self.noNotificationImageView.alpha == 1
        {
            visibility = (0, 1, 0)
        }
        else if self.sleepNotificationImageView.alpha == 1
        {
            visibility = (0, 0, 1)
        }
        
        Constants.currentOrigin = self.frame.origin
        Constants.currentTopLeftCornerRadius = self.topLeftView.layer.cornerRadius
        Constants.currentTopRightCornerRadius = self.topRightView.layer.cornerRadius
        Constants.currentBottomLeftCornerRadius = self.bottomLeftView.layer.cornerRadius
        Constants.currentBottomRightCornerRadius  = self.bottomRightView.layer.cornerRadius
        Constants.visibility = visibility
    }
    
    func moveNotificationBar()
    {
        if let origins = Constants.currentOrigin, let topLeftViewCornerRadius = Constants.currentTopLeftCornerRadius, let topRightViewCornerRadius = Constants.currentTopRightCornerRadius, let bottomLeftViewCornerRadius = Constants.currentBottomLeftCornerRadius, let bottomRightViewCornerRadius = Constants.currentBottomRightCornerRadius
        {
            self.frame.origin = origins
            self.topLeftView.layer.cornerRadius = topLeftViewCornerRadius
            self.topRightView.layer.cornerRadius = topRightViewCornerRadius
            self.bottomLeftView.layer.cornerRadius = bottomLeftViewCornerRadius
            self.bottomRightView.layer.cornerRadius = bottomRightViewCornerRadius
        }
        
        if let visibility = Constants.visibility
        {
            self.visibilityOfImages(notificationImageVisibility: CGFloat(visibility.0), noNotificationImageVisibility: CGFloat(visibility.1), sleepNotificationImageVisibility: CGFloat(visibility.2))
        }
    }
    
    @IBAction func didTouch()
    {
        self.superview!.bringSubviewToFront(self)
    }
    
    @IBAction func pressTheButton()
    {
        goToNotificationsButton.alpha = 0
        Constants.currentOrigin = self.frame.origin
        Constants.currentTopLeftCornerRadius = topLeftView.layer.cornerRadius
        Constants.currentTopRightCornerRadius = topRightView.layer.cornerRadius
        Constants.currentBottomLeftCornerRadius = bottomLeftView.layer.cornerRadius
        Constants.currentBottomRightCornerRadius = bottomRightView.layer.cornerRadius
        
        if didPanView.alpha == 0
        {
            didPanView.alpha = 1
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            self.frame.origin.x = self.superview!.frame.width / 2 - self.frame.width / 2
            self.frame.origin.y = self.superview!.frame.height / 2 - self.frame.height / 2
            self.topLeftView.layer.cornerRadius = self.frame.width / 2
            self.topRightView.layer.cornerRadius = self.frame.width / 2
            self.bottomRightView.layer.cornerRadius = self.frame.width / 2
            self.bottomLeftView.layer.cornerRadius = self.frame.width / 2
        }, completion: { (_) in
            self.animationNotificationView.alpha = 1
            self.topLeftView.layer.borderColor = UIColor.white.cgColor
            self.topRightView.layer.borderColor = UIColor.white.cgColor
            self.bottomLeftView.layer.borderColor = UIColor.white.cgColor
            self.bottomRightView.layer.borderColor = UIColor.white.cgColor
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                self.alpha = 0
                self.widthConstraint.constant = max(2 * self.superview!.frame.size.width, 2 * self.superview!.frame.size.height)
                self.animationNotificationView.layer.cornerRadius = self.widthConstraint.constant / 2
                self.superview!.layoutIfNeeded()
            }, completion: {(_) in
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
                    self.didPanView.alpha = 0
                    self.notificationView.alpha = 1
                }, completion: nil)
            })
        })
    }
    
    @IBAction func back()
    {
        if Constants.joinedNotify || Constants.connectedNotify || Constants.warningNotify ||  Constants.lostNotify || Constants.disconnectedNotify || Constants.messageNotify || Constants.usernameChangedNotify
        {
            visibilityOfImages(notificationImageVisibility: 0, noNotificationImageVisibility: 1, sleepNotificationImageVisibility: 0)
        }
        else
        {
            visibilityOfImages(notificationImageVisibility: 0, noNotificationImageVisibility: 0, sleepNotificationImageVisibility: 1)
        }
        didPanView.alpha = 1
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.notificationView.alpha = 0
        }, completion: { (_) in
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                self.alpha = 1
                self.widthConstraint.constant = 50
                self.animationNotificationView.layer.cornerRadius = 25
                self.superview!.layoutIfNeeded()
            }, completion: {(_) in
                self.animationNotificationView.alpha = 0
                self.topLeftView.layer.borderColor = UIColor.gray.cgColor
                self.topRightView.layer.borderColor = UIColor.gray.cgColor
                self.bottomLeftView.layer.borderColor = UIColor.gray.cgColor
                self.bottomRightView.layer.borderColor = UIColor.gray.cgColor
                UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                    self.frame.origin.x = Constants.currentOrigin.x
                    self.frame.origin.y = Constants.currentOrigin.y
                    self.topLeftView.layer.cornerRadius = Constants.currentTopLeftCornerRadius
                    self.topRightView.layer.cornerRadius = Constants.currentTopRightCornerRadius
                    self.bottomRightView.layer.cornerRadius = Constants.currentBottomRightCornerRadius
                    self.bottomLeftView.layer.cornerRadius = Constants.currentBottomLeftCornerRadius
                }, completion: {(_) in
                    self.goToNotificationsButton.alpha = 1
                    self.didPanView.alpha = 0
                })
            })
        })
    }
    
    @objc func didPan(gesture: UIPanGestureRecognizer)
    {
        if didPanView.alpha == 0
        {
            didPanView.alpha = 1
        }
        switch  gesture.state {
            
        case .began:
            initialOrigin = self.frame.origin
            
        case .changed:
            let translation = panGesture.translation(in: self.superview!)
            let newX = max(0, min(initialOrigin.x + translation.x, self.superview!.frame.width - self.frame.width))
            let newY = max(0, min(initialOrigin.y + translation.y, self.superview!.frame.height - self.frame.height))
            let newOrigin = CGPoint(x: newX, y: newY)
            self.frame.origin = newOrigin
            
            
            topLeftView.layer.cornerRadius = min(min(newOrigin.x, newOrigin.y), self.frame.width / 2)
            topRightView.layer.cornerRadius = min(min(self.superview!.frame.width - self.frame.width - newOrigin.x, newOrigin.y), self.frame.width / 2)
            bottomLeftView.layer.cornerRadius = min(min(newOrigin.x, self.superview!.frame.height - self.frame.height - newOrigin.y), self.frame.width / 2)
            bottomRightView.layer.cornerRadius = min(min(self.superview!.frame.width - self.frame.width - newOrigin.x, self.superview!.frame.height - self.frame.height - newOrigin.y), self.frame.width / 2)
            
        case .ended:
            let origin = self.frame.origin
            let size = self.frame.size
            
            var newTopLeftCornerRadius = self.frame.width / 2
            var newTopRightCornerRadius = self.frame.width / 2
            var newBottomLeftCornerRadius = self.frame.width / 2
            var newBottomRightCornerRadius = self.frame.width / 2
            var newOrigin = origin
            
            switch (origin.x < size.width / 2, origin.y < size.height / 2, self.superview!.frame.width - origin.x - size.width < size.width / 2, self.superview!.frame.height - origin.y - size.height < size.height / 2) {
            case (true, false, false, false):
                newTopLeftCornerRadius = 0
                newBottomLeftCornerRadius = 0
                newOrigin.x = 0
                
            case (false, true, false, false):
                newTopLeftCornerRadius = 0
                newTopRightCornerRadius = 0
                newOrigin.y = 0
                
            case (false, false, true, false):
                newTopRightCornerRadius = 0
                newBottomRightCornerRadius = 0
                newOrigin.x = self.superview!.frame.width - size.width
                
            case (false, false, false, true):
                newBottomRightCornerRadius = 0
                newBottomLeftCornerRadius = 0
                newOrigin.y = self.superview!.frame.height - size.height
                
            case (true, true, false, false):
                newTopRightCornerRadius = 0
                newTopLeftCornerRadius = 0
                newBottomLeftCornerRadius = 0
                newOrigin = .zero
                
            case (true, false, false, true):
                newTopLeftCornerRadius = 0
                newBottomLeftCornerRadius = 0
                newBottomRightCornerRadius = 0
                newOrigin.x = 0
                newOrigin.y = self.superview!.frame.height - size.height
                
            case (false, true, true, false):
                newTopRightCornerRadius = 0
                newTopLeftCornerRadius = 0
                newBottomRightCornerRadius = 0
                newOrigin.x = self.superview!.frame.width - size.width
                newOrigin.y = 0
                
            case (false, false, true, true):
                newTopRightCornerRadius = 0
                newBottomRightCornerRadius = 0
                newBottomLeftCornerRadius = 0
                newOrigin.x = self.superview!.frame.width - size.width
                newOrigin.y = self.superview!.frame.height - size.height
                
            default:
                break
            }
            
            UIView.animate(withDuration: 0.5,
                           delay: 0,
                           options: .curveEaseOut,
                           animations: {
                            self.frame.origin = newOrigin
                            self.topLeftView.layer.cornerRadius = newTopLeftCornerRadius
                            self.topRightView.layer.cornerRadius = newTopRightCornerRadius
                            self.bottomLeftView.layer.cornerRadius = newBottomLeftCornerRadius
                            self.bottomRightView.layer.cornerRadius = newBottomRightCornerRadius
            }, completion: {(_) in
                self.didPanView.alpha = 0
            })
            
            
        default:
            break
        }
    }
    
    //MARK:- Functions
    func visibilityOfImages(notificationImageVisibility: CGFloat, noNotificationImageVisibility: CGFloat, sleepNotificationImageVisibility: CGFloat)
    {
        self.notificationImageView.alpha = notificationImageVisibility
        self.noNotificationImageView.alpha = noNotificationImageVisibility
        self.sleepNotificationImageView.alpha = sleepNotificationImageVisibility
    }
    
    func updateNotificationBar()
    {
        if Constants.notifications.count == 0
        {
            self.goToNotificationsButton.alpha = 0
            self.frame.origin.x = superview!.frame.size.width - 50
            self.alpha = 0
            self.goToNotificationsButton.alpha = self.alpha
        }
        else
        {
            if Constants.joinedNotify || Constants.connectedNotify || Constants.warningNotify ||  Constants.lostNotify || Constants.disconnectedNotify || Constants.messageNotify || Constants.usernameChangedNotify
            {
                visibilityOfImages(notificationImageVisibility: 1, noNotificationImageVisibility: 0, sleepNotificationImageVisibility: 0)
            }
            else
            {
                visibilityOfImages(notificationImageVisibility: 0, noNotificationImageVisibility: 0, sleepNotificationImageVisibility: 1)
            }
            self.goToNotificationsButton.alpha = 0
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
                self.alpha = 1 - self.animationNotificationView.alpha
            }, completion: {(_) in
                self.goToNotificationsButton.alpha = self.alpha
            })
        }
    }
}
