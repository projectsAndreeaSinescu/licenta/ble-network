//
//  ViewController.swift
//  BLE Network With Server
//
//  Created by Andreea Sinescu on 26/04/2019.
//  Copyright © 2019 Andreea Sinescu. All rights reserved.
//

import UIKit
import SwiftMessages

class LoginViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var arrowImage: UIImageView!
    
    var deviceID: String?
    weak var internetConnectionView: MessageView!
    
    //MARK:- View functions
    override func viewDidLoad()
    {
        super.viewDidLoad()
        userNameTextField.layer.borderWidth = 1
        userNameTextField.layer.cornerRadius = userNameTextField.frame.size.height / 2
        userNameTextField.clipsToBounds = true
        nextButton.layer.cornerRadius = nextButton.frame.size.height / 2
        nextButton.clipsToBounds = true
        
        deviceID = UserDefaults.standard.string(forKey: "deviceID")
        let username = UserDefaults.standard.string(forKey: "username")
        if username != nil
        {
            userNameTextField.text = username
        }
        
        if deviceID == nil || deviceID!.isEmpty
        {
            let uuid = String(UUID().uuidString.prefix(23))
            deviceID = uuid
            UserDefaults.standard.set(uuid, forKey: "deviceID")
        }
        
        CoreDataController.sharedInstance.deleteAllObjects(forEntity: "User")
        CoreDataController.sharedInstance.deleteAllObjects(forEntity: "Message")
        print(WebService.sharedInstance.reachabilityManager.isReachable)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        let username = UserDefaults.standard.string(forKey: "username")
        if username != nil
        {
            userNameTextField.text = username
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeConnexion(notification:)), name: connexionDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Create view controller
    static func createViewController() -> LoginViewController
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        return controller
    }
    
    //MARK:- Actions
    @IBAction func login()
    {
        if let username = userNameTextField.text, !(username.isEmpty), let deviceID = deviceID
        {
            let usernameWithoutSpaces = username.split(separator: " ")
            if usernameWithoutSpaces.count == 0
            {
                showErrors(title: "User name error", bodyText: "Username must contain at least one unblank character!", theme: .warning)
                return
            }
            userNameTextField.resignFirstResponder()
            self.view.showLoadingIndicator()
            WebService.sharedInstance.login(deviceID: deviceID, username: username) { (response) in
                guard response.isSuccess else
                {
                    self.view.hideLoadingIndicator(animated: true)
                    self.showErrors(title: "Attention", bodyText:  response.errorMessage ?? "Failed to login!", theme: .error)
                    return
                }
                UserDefaults.standard.set(username, forKey: "username")
                self.view.hideLoadingIndicator(animated: false)
                let controller = MenuViewController.createViewController()
                self.present(controller, animated: true, completion: nil)
            }
        }
        else
        {
            showErrors(title: "User name missing", bodyText: "Complete user name field!", theme: .warning)
        }
    }
    
    //MARK:- Functions
    @objc func didChangeConnexion(notification: Notification)
    {
        guard let hasConnection = notification.object as? Bool else {
            return
        }
        
        if !hasConnection
        {
            showInternetConnectionError()
        }
        else
        {
            hideInternetConnectionError()
        }
    }
    
    func showInternetConnectionError()
    {
        internetConnectionView = MessageView.viewFromNib(layout: .statusLine)
        internetConnectionView.configureTheme(.error)
        internetConnectionView.configureDropShadow()
        internetConnectionView.configureContent(body: "Please check your internet connection!")
        internetConnectionView.id = "internetConnectionView"
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.duration = .forever
        config.interactiveHide = false
        
        SwiftMessages.show(config: config, view: internetConnectionView)
        nextButton.isEnabled = false
        arrowImage.alpha = 0.5
        userNameTextField.resignFirstResponder()
        userNameTextField.isEnabled = false
    }
    
    func hideInternetConnectionError()
    {
        SwiftMessages.hide(id: "internetConnectionView")
        internetConnectionView = nil
        nextButton.isEnabled = true
        arrowImage.alpha = 1
        userNameTextField.isEnabled = true
    }
    
    func showErrors(title: String, bodyText: String, theme: Theme)
    {
        let errorView = MessageView.viewFromNib(layout: .cardView)
        errorView.configureTheme(theme)
        errorView.configureDropShadow()
        errorView.configureContent(title: title, body: bodyText)
        errorView.id = "errorView"
        errorView.button?.removeFromSuperview()
        
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.duration = .automatic
        config.interactiveHide = true
        
        SwiftMessages.show(config: config, view: errorView)
    }
}

extension LoginViewController: UITextFieldDelegate
{
    //MARK: - Text Field Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        login()
        return true
    }
}

extension UIViewController
{
    //MARK: - Alert
    func showAlert(title: String, message: String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
