# BLE Network

## Ce este BLE Network?

În acest univers în care au fost create atât de multe aplicații care utilizează Bluetooth Low Energy, am pornit de la ideea că una dintre portițele către lumea reală poate fi o 
excursie în grup la munte sau oriunde altundeva. În drumețiile organizate, pot apărea situații neprevăzute precum pierderea unui om din grup fără a știi unde a fost văzut ultima
oară cu exactitate, accidentarea unei persoane fără ca grupul să observe sau dorința de a știi că o persoană este în siguranță chiar dacă nu poți lua legătura cu ea. 

Din acest motiv am ales să creez o aplicație mobilă pentru dispozitivele iOS care să creeze cu ajutorul Bluetooth-ului o rețea între utilizatorii aplicației, iar atunci când un 
utilizator nu mai este văzut de niciun membru al aplicației, se va genera o alarmă care să atenționeze că persoana respectivă este pierdută. Numele aplicației este „BLE Network”
și este dezvoltată folosind limbajul de programare Swift specific aplicațiilor mobile iOS și design pattern-ul MVC (Model-View-Controller). Am ales acest timp de aplicație pentru
a ajuta persoanele care pleacă în excursii cu un grup mare de oameni și care își doresc să știe dacă tovarășii lor sunt în siguranță, iar în cazul pierderii unui membru să poată
acționa imediat îndrumat de aplicație.

Am ales în cadrul aplicației să folosesc Bluetooth-ul pentru a scana dispozitivele din apropiere, iar mai apoi să transmit informațiile colectate către server prin conexiune la 
internet pentru a fi prelucrate și interpretate, mai precis am utilizat Bluetooth Low Energy (BLE) pentru a optimiza consumul de energie a baterie telefonului pe care rulează 
aplicația. 


## Scopul aplicației

Scopul aplicației este de a menține un grup unit și în siguranță, oriunde s-ar desfășura activitățile acestuia. De exemplu, un mediu prielnic folosirii aplicației este o excursie 
la munte. Această aplicație are la bază crearea unui graf al conexiunilor dintre utilizatori cu ajutorul scanării prin Bluetooth, ceea ce o deosebește de restul aplicațiilor 
disponibile pe piață. Aceasta ușurează organizarea excursiilor și drumețiilor într-un grup mai mare de oameni.

## Ecosistemul aplicației
Pentru a înțelege mai bine modul în care aplicația a fost construită trebuie să privim asupra
ecosistemului aplicației (din imaginea de mai jo). Atât dezvoltatorul, cât și dispozitivul pe care
rulează aplicația mobilă comunică cu zona de Amazon Web Services (AWS) Cloud. Această zonă 
cuprinde server-ul, S3-ul, baza de date și git-ul. Prin intermediul contului de git, 
dezvoltatorul actualizează deployment pipeline-ul care comunică mai departe cu server. 
Server-ul transmite informații către S3 și către baza de date folosind anumite credențiale
specifice pentru a asigura accesul. De asemenea, dezvoltatorul poate intra prin același mod 
în legătură cu S3-ul și cu baza de date introducând contul de utilizator. Mai departe, 
aplicația mobilă poate accesa prin intermediul access token-ului server-ul care la rândul 
lui transmite informații către Firebase folosindu-se de credențialele specifice. Cu ajutorul 
certificatelor,    Firebase-ul comunică cu Apple-ul care transmite mai departe către dispozitiv 
informațiile. Dispozitivul accesează Firebase-ul la rândul lui prin SDK.
![](docs/architecture-diagram.png)

## Cazurile de utilizare a aplicației
O altă activitate extrem de importantă desfășurată în cadrul aplicației 
noastre este scanare și actualizarea vecinilor utilizatorului (prezentată 
în imaginea de mai jos) care este posibilă doar după autentificare. Aplicația 
mobilă inițiază acest proces, apoi cere sistemului să îi raporteze status-ul 
Bluetooth-ului, iar în urma primirii acestuia pornește scanarea propriu-zisă,
sistemul trimițând înapoi aplicației dispozitivele pe care le-a descoperit în
apropiere. Se trimite actualizarea vecinilor către API care transmite mai 
departe informațiile primite către baza de date, notifică schimbările de 
stări ale utilizatorilor aplicației prin Firebase și trimite către 
aplicație utilizatorul curent cu informațiile actualizate. La final, 
aplicația actualizează interfața pe care o vede utilizatorul.
![](docs/bluetooth-case-diagram.png)

Procesul de actualizare a locației se aseamănă cu cel de 
scanare a vecinilor și este la fel de important în cadrul aplicației. 
Aplicația mobilă inițiază acest proces și cere informații referitoare 
la status-ul GPS-ului sistemului care îi va răspunde. În cazul actualizării 
locației, sistemul se autodeclanșează și transmite noua poziționare către 
aplicația mobilă care o transmite mai departe către server. Server-ul 
trimite noua locație către baza de date pentru a o actualiza, iar apoi 
notifică schimbarea celorlalți utilizatori și transmite mai departe user-ul 
actualizat înapoi către aplicație. În final, se actualizează UI-ul. 
![](docs/location-case-diagram.png)

În cazul trimiterii mesajelor, avem 2 tipuri de mesaje: mesaje care conțin 
o imagine (mesajele de tip imagine sau coordonate) și cele care nu conțin 
(mesajele de tip text). Singura diferență dintre acestea fiind salvarea 
imaginii, de aceea voi prezenta cazul în care se trimite și o poză alături de 
conținut. Utilizatorul trimite conținutul și imaginea către server. Server-ul 
transmite imaginea către S3 care o salvează în cloud și răspunde printr-un url 
prin care se poate accesa imaginea.  Conținutul specific fiecărui tip de mesaj 
este trimis alături de tip către baza de date pentru a se asigura persistența 
datelor primite și procesate, iar aceasta răspunde cu mesajul creat care 
conține toate detaliile specifice. Mai apoi API-ul transmite notificări către 
ceilalți utilizatori ai aplicației referitoare la apariția unui nou mesaj în 
cadrul chat-ului. Mesajul primit de la baza de date este transmis către 
aplicație prin intermediul server-ului. Aplicația actualizează interfața 
user¬-ului pe care i-o afișează utilizatorului. Astfel, se încheie procesul 
de trimitere al mesajelor în cadrul aplicației. Această activitate menține 
grupul unit și creează un mediu prielnic activităților pe care un grup mare 
de oameni le poate desfășurare. Cu ajutorul mesajelor este posibilă și 
trimiterea locației curente care poate ajuta la găsirea unei persoane 
accidentate. 
De asemenea, dacă un utilizator autentificat pierde conexiunea la internet, 
el va putea vizualiza în continuare ultimele date pe care le-a primit de 
la server, dar nu va mai primi notificări privitoare la noile informații 
pe care le-a generat sistemul în urma interacțiunii cu aplicația și nu va 
mai putea trimite mesaje.
![](docs/messages-case-diagram.png)


## Perspectiva comportamentală a aplicației
Pentru o afișare cât mai reprezentativă a stărilor prin care poate trece un
utilizator în procesul de utilizare a aplicației, am utilizat acest tip de
diagramă care se poate vizualiza în imaginea de mai jos.
După ce utilizatorul deschide aplicația și reușește să se autentifice acesta 
se află în starea connecting corespunzătoare status-ului 0 care simbolizează 
faptul că utilizatorul de abia s-a alăturat grupului și urmează să transmită 
lista de vecini cu care se conectează. În cazul în care lista de vecini nu 
este goală atunci din starea connecting, warning sau lost utilizatorul ajunge 
în starea cu numărul 1 și anume connected, iar în caz contrar, va fi declarat 
pierdut și va avea status-ul lost. În cazul în care un utilizator nu a mai 
realizat nicio actualizare de 60 de secunde, acesta va fi declarat ca având 
starea warning, adică este posibil să fie în pericol. Dacă un utilizator 
declarat warning nu trimite nicio actualizare de 300 de secunde, atunci va fi 
anunțat ca fiind pierdut (lost). Prin inițierea procesului de logout, din 
orice stare în care se află un utilizator autentificat va trece în starea 
disconnected care simbolizează faptul că a părăsit grupul de bunăvoie. 

![](docs/state-diagram.png)

## Ecrane ale aplicației

Login | Meniu | Setari
:-------------------------:|:-------------------------:|:-------------------------:
![](./docs/Login.jpeg) | ![](./docs/Menu.jpeg) | ![](./docs/settings.jpeg)

Radar conexiuni | Lista de conexiuni 
:-------------------------:|:-------------------------:
![](./docs/radar.jpeg) | ![](docs/connections-list.jpeg) 

Chat | Harta locatii utilizatori
:-------------------------:|:-------------------------:
![](./docs/messages.jpeg) | ![](./docs/map.jpeg) 

